// This is a utility function to get query parameters
// Usage: $.QueryString['get_parameter']
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'));
})(jQuery);

//inserting commas in numbers.
(function($) {
	$.insertThousandCommaSeparator = function( nInput ) {
		
		if ( !isNaN( nInput ) ) {
			var s = new String( nInput );
			var num = s.split( "." );
			var l = num[0].length;
			var res = '' + num[0][0];
			for ( var i = 1; i <= l-1; i++ ) {
				if ( (l-i)%3 == 0 ) res += ',';
				res += num[0][i];
			}
			num[0] = res;
			return num.join( "." ).replace( "-,", "-" );
		}
		return 0;
	};
})(jQuery);

//removing commas in computing numbers (float).
(function($) {
	$.removeFloatCommas = function(s) {	
		var num = 0;
		if ( s !== undefined ) {
			num = parseFloat( s.replace(/,/g, '') );
			
			if ( isNaN( num ) ) {
				num = 0;
			}
		}

		return num;
	};
})(jQuery);

//removing commas in computing numbers (integer).
(function($) {
	$.removeIntCommas = function(s) {
		var num = 0;
		if ( s !== undefined ) {
			num = parseInt( s.replace(/,/g, '') );
	
			if ( isNaN( num ) ) {
				num = 0;
			}
		}
		
		return num;
	};
})(jQuery);

(function($) {
	Number.prototype.round = function() {
		return Math.round( this );
	};
})(jQuery);

(function($) {
	$.roundDecimal = function(amount, digits) {
		digits = digits || 2;
		return Math.round(amount * Math.pow(10, digits)) / Math.pow(10, digits);
	};
})(jQuery);

(function($) {
	$( '#content-wrapper' ).on( 'submit', 'form', function() {
		window.onbeforeunload = null;
	    var btn = $( this ).find( "button.submit-button" );
	    btn.button('loading');
	    btn.prepend( '<i class="fa fa-spinner fa-spin"></i> ');
	    setTimeout( function () {
	    	btn.find( '.fa' ).hide();
	    	btn.button( "reset" );
	    }, 99999);
	});
})(jQuery);

(function($) {
	$.addCommas = function(str) {
		if ( !isNaN( str) ) {
			var amount = new String(str);
		    amount = amount.split("").reverse();

		    var output = "";
		    for ( var i = 0; i <= amount.length-1; i++ ){
		        output = amount[i] + output;
		        if ((i+1) % 3 == 0 && (amount.length-1) !== i)output = ',' + output;
		    }
		    return output;
		}
		return 0;
	};
})(jQuery);

(function($) {
	$( ".select2" ).select2();
})(jQuery);

//gets arrayObject size
(function($) {
	$.arraySize = function(obj) {
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};
})(jQuery);

//format date MM/DD/YYYY
(function($) {
	$.formatDate = function(date) {
	    var dateObj = new Date(date);
	    return (dateObj.getMonth() + 1) + "/" + dateObj.getDate() + "/" + dateObj.getFullYear();
	};
})(jQuery);

//format null to ""
(function($) {
	$.formatNull = function(data) {
	    if (data == null) {
	    	return "";
	    } else {
	    	return data;
	    }
	};
})(jQuery);

//prompts user if user confirms leaving the page when form has dirty data
(function($) {
	$( "#content-wrapper" ).find( "form:not( .search-form )" ).find( "input, select, textarea" ).on( "change", function () {
		window.onbeforeunload = function() {
			return "You have not yet saved the changes you have made.";
		};
	});
})(jQuery);

//nl2br function
(function($) {
	$.nl2br = function(str, is_xhtml) {
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
		
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}
})(jQuery);

//for rounding amount to specified number of decimal digits
(function($) {
 $.roundDecimal = function(amount, digits) {
  digits = digits || 2;
  return Math.round(amount * Math.pow(10, digits)) / Math.pow(10, digits);
 };
})(jQuery);

//timeago
(function($) {
	$.timeago.settings.cutoff = 31557600000; //milliseconds
	$( ".timeago" ).timeago();
})(jQuery);