var chainCtrl = {
	init: function() {
		chainCtrl.toggleParentGroup();
		
		$( "input.is-parent" ).change( function() {
			chainCtrl.toggleParentGroup();
		});
	},
	
	toggleParentGroup: function() {
		$( ".parent-container" ).removeClass( "hide" ).show();
		$( ".parent-container select" ).attr( "required", true );
		$( ".parent-container .optional" ).remove();
		
		if ( $( "input.is-parent" ).is( ":checked" ) ) {
			$( ".parent-container" ).hide();
			$( ".parent-container select" ).removeAttr( "required" );
		}
	}
};

$( document ).ready( function() {
	chainCtrl.init();
});