var sidebar = {
	sidebarRequest: null,
	init: function() {
		$( "#main-menu-toggle" ).on( "click", function() {
			if ( sidebar.sidebarRequest ) {
				sidebar.sidebarRequest.abort();
				sidebar.sidebarRequest = null;
			}

			sidebar.sidebarRequest = $.ajax({
				type: "POST",
				url: app.base_uri + "/admin/user/sidebar-collapsed",
				data: { "sidebarCollapsed" : localStorage.getItem( "pa_mmstate" ) }
			}).done( function() {
				sidebar.sidebarRequest = null;
			});
		});
	}
};

init.push( function() {
	sidebar.init();
});