<?php

namespace CI\CoreBundle\Classes;

use SimpleThings\EntityAudit\AuditReader as BaseAuditReader;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Common\Collections\ArrayCollection;
use SimpleThings\EntityAudit\Metadata\MetadataFactory;
use SimpleThings\EntityAudit\ChangedEntity;
use SimpleThings\EntityAudit\AuditConfiguration;
use SimpleThings\EntityAudit\Utils\ArrayDiff;
use SimpleThings\EntityAudit\AuditException;

class AuditReader extends BaseAuditReader
{
	protected $em;
	
	protected $config;
	
	protected $metadataFactory;
	
	/**
	 * @param EntityManager $em
	 * @param AuditConfiguration $config
	 * @param MetadataFactory $factory
	 */
	public function __construct(EntityManager $em, AuditConfiguration $config, MetadataFactory $factory)
	{
		// need to call parent constructor because superclass variables are private 
		parent::__construct($em, $config, $factory);
		
		$this->em = $em;
		$this->config = $config;
		$this->metadataFactory = $factory;
		$this->platform = $this->em->getConnection()->getDatabasePlatform();
	}
	
	/**
	 * Find a class at the specific revision.
	 *
	 * This method does not require the revision to be exact but it also searches for an earlier revision
	 * of this entity and always returns the latest revision below or equal the given revision
	 *
	 * @param string $className
	 * @param mixed $id
	 * @param int $revision
	 * @param boolean $flag
	 * @return object
	 */
	public function find($className, $id, $revision, $flag = true)
	{
		if (!$this->metadataFactory->isAudited($className)) {
			throw AuditException::notAudited($className);
		}
	
		$class = $this->em->getClassMetadata($className);
		$tableName = $this->config->getTablePrefix() . $class->table['name'] . $this->config->getTableSuffix();
	
		if (!is_array($id)) {
			$id = array($class->identifier[0] => $id);
		}
	
		$whereSQL  = "e." . $this->config->getRevisionFieldName() ." <= ?";
	
		foreach ($class->identifier AS $idField) {
			if (isset($class->fieldMappings[$idField])) {
				$columnName = $class->fieldMappings[$idField]['columnName'];
			} else if (isset($class->associationMappings[$idField])) {
				$columnName = $class->associationMappings[$idField]['joinColumns'][0];
			}
	
			$whereSQL .= " AND " . $columnName . " = ?";
		}
	
		$columnList = "";
		$columnMap  = array();
	
		foreach ($class->fieldNames as $columnName => $field) {
			if ($columnList) {
				$columnList .= ', ';
			}
	
			$type = Type::getType($class->fieldMappings[$field]['type']);
			$columnList .= $type->convertToPHPValueSQL(
					$class->getQuotedColumnName($field, $this->platform), $this->platform) .' AS ' . $field;
			$columnMap[$field] = $this->platform->getSQLResultCasing($columnName);
		}
	
		foreach ($class->associationMappings AS $assoc) {
			if ( ($assoc['type'] & ClassMetadata::TO_ONE) == 0 || !$assoc['isOwningSide']) {
				continue;
			}
	
			foreach ($assoc['targetToSourceKeyColumns'] as $sourceCol) {
				if ($columnList) {
					$columnList .= ', ';
				}
	
				$columnList .= $sourceCol;
				$columnMap[$sourceCol] = $this->platform->getSQLResultCasing($sourceCol);
			}
		}
	
		$values = array_merge(array($revision), array_values($id));
	
		$query = "SELECT " . $columnList . " FROM " . $tableName . " e WHERE " . $whereSQL . " ORDER BY e.rev DESC";
		$row = $this->em->getConnection()->fetchAssoc($query, $values);
	
		if (!$row) {
			//flag determines if this function should throw an exception, default value is true.
			if ($flag === true) {
				throw AuditException::noRevisionFound($class->name, $id, $revision);
			} else {
				return null;
			}
		}
	
		return $this->createEntity($class->name, $row);
	}
	
	/**
	 * Copied from SimpleThings\EntityAudit\AuditReader because function was declared as private
	 *
	 * @param string $className
	 * @param array $data
	 * @return object
	 */
	protected function createEntity($className, array $data)
	{
		$class = $this->em->getClassMetadata($className);
		$entity = $class->newInstance();
	
		foreach ($data as $field => $value) {
			if (isset($class->fieldMappings[$field])) {
				$type = Type::getType($class->fieldMappings[$field]['type']);
				$value = $type->convertToPHPValue($value, $this->platform);
				$class->reflFields[$field]->setValue($entity, $value);
			}
		}
	
		foreach ($class->associationMappings as $field => $assoc) {
			// Check if the association is not among the fetch-joined associations already.
			if (isset($hints['fetched'][$className][$field])) {
				continue;
			}
	
			$targetClass = $this->em->getClassMetadata($assoc['targetEntity']);
	
			if ($assoc['type'] & ClassMetadata::TO_ONE) {
				if ($assoc['isOwningSide']) {
					$associatedId = array();
					foreach ($assoc['targetToSourceKeyColumns'] as $targetColumn => $srcColumn) {
						$joinColumnValue = isset($data[$srcColumn]) ? $data[$srcColumn] : null;
						if ($joinColumnValue !== null) {
							$associatedId[$targetClass->fieldNames[$targetColumn]] = $joinColumnValue;
						}
					}
					if ( ! $associatedId) {
						// Foreign key is NULL
						$class->reflFields[$field]->setValue($entity, null);
					} else {
						$associatedEntity = $this->em->getReference($targetClass->name, $associatedId);
						$class->reflFields[$field]->setValue($entity, $associatedEntity);
					}
				} else {
					// Inverse side of x-to-one can never be lazy
					$class->reflFields[$field]->setValue($entity, $this->getEntityPersister($assoc['targetEntity'])
							->loadOneToOneEntity($assoc, $entity));
				}
			} else {
				// Inject collection
				$reflField = $class->reflFields[$field];
				$reflField->setValue($entity, new ArrayCollection);
			}
		}
	
		return $entity;
	}
	
	
	
    /**
     * Override findEntitiesChangedAtRevision()
     * We can now limit which audited entities to search by passing an array of classes  
     *
     * @param int $revision
     * @param array $auditedEntities
     * @return ChangedEntity[]
     */
	public function findEntitiesChangedAtRevision($revision, $auditedEntities = null)
    {
    	if (is_null($auditedEntities)) {
    		$auditedEntities = $this->metadataFactory->getAllClassNames();
    	}
    	
    	$changedEntities = array();
    	$counter = 0;
    	foreach ($auditedEntities AS $className) {
    		$class = $this->em->getClassMetadata($className);
    		$tableName = $this->config->getTablePrefix() . $class->table['name'] . $this->config->getTableSuffix();
    
    		$whereSQL   = "e." . $this->config->getRevisionFieldName() ." = ?";
    		$columnList = "e." . $this->config->getRevisionTypeFieldName();
    		$columnMap  = array();
    
    		foreach ($class->fieldNames as $columnName => $field) {
    			$type = Type::getType($class->fieldMappings[$field]['type']);
    			$columnList .= ', ' . $type->convertToPHPValueSQL(
    				$class->getQuotedColumnName($field, $this->platform), $this->platform) . ' AS ' . $field;
    			$columnMap[$field] = $this->platform->getSQLResultCasing($columnName);
    		}
    
    		foreach ($class->associationMappings AS $assoc) {
    			if ( ($assoc['type'] & ClassMetadata::TO_ONE) > 0 && $assoc['isOwningSide']) {
    				foreach ($assoc['targetToSourceKeyColumns'] as $sourceCol) {
    					$columnList .= ', ' . $sourceCol;
    					$columnMap[$sourceCol] = $this->platform->getSQLResultCasing($sourceCol);
    				}
    			}
    		}
    
    		$this->platform = $this->em->getConnection()->getDatabasePlatform();
    		$query = "SELECT " . $columnList . " FROM " . $tableName . " e WHERE " . $whereSQL;

    		$revisionsData = $this->em->getConnection()->executeQuery($query, array($revision));
    
    		foreach ($revisionsData AS $row) {
    			$id   = array();
    			$data = array();
    
    			foreach ($class->identifier AS $idField) {
    				$id[$idField] = $row[$idField];
    			}
    
    			$entity = $this->createEntity($className, $row);
    			$changedEntities[] = new ChangedEntity($className, $id, $row[$this->config->getRevisionTypeFieldName()], $entity);
    		}
    		
    		//checks if the class name is child or not. Second class should be the child if the parent is a nested collection.
    		$counter++;
    	}
    	return $changedEntities;
    }
    
    /**
     * Get an array with the differences of between two specific revisions of
     * an object with a given id.
     *
     * @param string $className
     * @param int $id
     * @param int $oldRevision
     * @param object $newObject
     * @return array
     */
    public function diff($className, $id, $oldRevision, $newObject)
    {
    	$oldValues = array();
    	$newValues = array();
    	
    	$oldObject = $this->find($className, $id, $oldRevision, false);
    	if ($oldObject) {
    		$oldValues = $oldObject->getLog();
    	}

    	$newValues = $newObject->getLog();
    
    	$differ = new ArrayDiff();
    	return $differ->diff($oldValues, $newValues);
    }
}