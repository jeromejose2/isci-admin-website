<?php

namespace CI\CoreBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;

use CI\CoreBundle\Entity\User;
use CI\CoreBundle\Form\EventListener\AddRoleFieldSubscriber;
use Symfony\Component\Validator\Constraint;

class CustomProfileFormType extends AbstractType
{
	protected $securityContext;
	protected $userId;
	
	public function __construct(SecurityContext $securityContext, $userId)
	{
		$this->securityContext = $securityContext;
		$this->userId = $userId;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
// 		$subscriber = new AddRoleFieldSubscriber($builder->getFormFactory(), $this->securityContext);
// 		$builder->addEventSubscriber($subscriber);
		$builder
		->setMethod('PUT')
		->add('firstName', 'text', array(
			'label' => 'First Name'
		))
		->add('lastName', 'text', array(
			'label' => 'Last Name',
			'required' => false
		))
		->add('username')
		->add('email', 'email')
		->add('gender', 'choice', array(
			'expanded' => true,
			'attr' => array('inline' => true),
			'choices' => array(
				'M' => 'Male', 
				'F' => 'Female'
			)
		))
		->add('birthDate', 'date', array(
			'label'    => 'Birthday',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('contactNumber', 'text', array(
			'label' => 'Contact Number',
			'required' => false
		))
		->add('formRole', 'choice', array(
			'required' => true,
			'label' => 'Role',
			'empty_value' => 'Choose a role',
			'attr' => array('class' => 'role'),
			'constraints' => array(new NotBlank(array('message' => 'Please choose a role for this user'))),
			'choices' => array(
				User::ROLE_ADMIN => 'Admin',
				User::ROLE_IT_ACCOUNT => 'IT Account',
				User::ROLE_RSM => 'RSM',
				User::ROLE_CDM => 'CDM',
				User::ROLE_TL => 'TL',
				User::ROLE_DISER => 'DISER'
			)
		))
		;
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\CoreBundle\Entity\User',
			'validation_groups' => array("Default", "ValidateRole")
		));
	}
	
	public function getName()
	{
		return 'ci_core_custom_user_edit_profileform';
	}
}