<?php
namespace CI\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CI\CoreBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		// username, email, password, enable?, role, firstname, lastname, contactnumber, gender, birthday
		
		$users = array(
			array('admin', 'admin@creatinginfo.com', 'admin', true, array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'),'admin', 'admin', '0917-999-9999', 'M', new \DateTime('now')),
		);
		
		for ($row = 0; $row < 1; $row++) {
			$user = new User();
			$user->setUsername($users[$row][0]);
			$user->setEmail($users[$row][1]);
			$user->setPlainPassword($users[$row][2]);
			$user->setEnabled($users[$row][3]);
			$user->setRoles($users[$row][4]);
			$user->setFirstName($users[$row][5]);
			$user->setLastName($users[$row][6]);
			$user->setContactNumber($users[$row][7]);
			$user->setGender($users[$row][8]);
			$user->setBirthDate($users[$row][9]);
			$user->setCreatedAt(new \DateTime('now'));
			$user->setCreatedBy("admin");
			$manager->persist($user);
			$manager->flush();
		}		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 1; // the order in which fixtures will be loaded
	}
}
