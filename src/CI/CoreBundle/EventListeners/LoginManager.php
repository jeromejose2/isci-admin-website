<?php
namespace CI\CoreBundle\EventListeners;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine; 
use FOS\UserBundle\Event\FormEvent;
use CI\CoreBundle\Entity\UserLog;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class LoginManager implements EventSubscriberInterface
{
	/** @var \Symfony\Component\Security\Core\SecurityContext */
	private $securityContext;

	/** @var \Doctrine\ORM\EntityManager */
	private $em;

    /**
     * Constructor
     *
     * @param SecurityContext $securityContext
     * @param Doctrine        $doctrine
     */
	public function __construct(SecurityContext $securityContext, Doctrine $doctrine)
	{
		$this->securityContext = $securityContext;
     	$this->em = $doctrine->getEntityManager();
    }
    
    public static function getSubscribedEvents()
    {
    	return array();
    }
    
    public function onSecurityLogin(InteractiveLoginEvent $event)
    {
    	if ($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
        	$user = $event->getAuthenticationToken()->getUser();
        	$request = $event->getRequest();
        	
        	$this->saveLogin($request, $user);
        }
    }
    
    public function saveLogin($request, $user)
    {
        $log = new UserLog();
        $log->setUser($user);
        $log->setIp($request->getClientIp());
        $log->setLoggedAt(new \DateTime());
        
        $this->em->persist($log);
        $this->em->flush();
    }
}