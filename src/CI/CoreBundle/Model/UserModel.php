<?php 

namespace CI\CoreBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\CoreBundle\Form\Type\CustomProfileFormType;
use CI\CoreBundle\Form\Type\UserFilterType;
use CI\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserModel
{
	const REPOSITORY = 'CICoreBundle:User';
	const ACTION_UPDATE = 'update';
	const ACTION_DEACTIVATE = 'deactivate';
	const ACTION_REACTIVATE = 'reactivate';

	private $container;
	private $formFactory;
	private $em;
	private $securityContext;

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
		$this->securityContext = $container->get('security.context');
	}

	public function findExistingEntity($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->find($id);
	}
	
	public function getFilterFormType()
	{
		return $this->formFactory->create(new UserFilterType(), null, array('method' => 'GET'));
	}

	public function getType(User $entity = null)
	{
		if (!empty($entity)) {
			$role = null;
			foreach ($entity->getRoles() as $role) {
				if ($role != User::ROLE_SUPER_ADMIN) {
					$entity->setFormRole($role);
					break;
				}
			}
		}
		
		$form = $this->formFactory->create(new CustomProfileFormType($this->securityContext, empty($entity) ? 0 : $entity->getId()), $entity);
		
		return $form;
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_UPDATE: 
				return 'User has been updated successfully.';
			case self::ACTION_DEACTIVATE:
				return 'User has been deactivated.';
			case self::ACTION_REACTIVATE:
				return 'User has been reactivated.';
			case 'delete':
				return 'User has been deleted.';
		}
	}
	
	public function isDeletable($entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This user has already been added to a store or device and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'user_delete',
			'return_path' => 'user_show',
			'name' => '[User] ' . $entity->getName()
		);
	}
	
	public function deleteEntity($id)
	{
		$um = $this->container->get('fos_user.user_manager');
		$user = $this->findExistingEntity($id);
		$um->deleteUser($user);
	}
	
	public function checkSuperRole(User $entity)
	{
		if ($entity->hasRole(User::ROLE_SUPER_ADMIN)) {
			return true;
		}
		
		return false;
	}

	public function getIndex($params = null)
	{
		return $this->em->getRepository(self::REPOSITORY)->findAll($params);
	}

	public function saveEntity(Form $form, User $entity, $isSuper = false)
	{
		if ($isSuper === true) {
			$entity->addRole(User::ROLE_SUPER_ADMIN);
		}
		
		$this->em->persist($entity);
		$this->em->flush();
	}

	public function manageAccess($id, $access)
	{
		$entity = $this->findExistingEntity($id);
		$access == User::ACCESS_DEACTIVATE ? $entity->setEnabled(false) : $entity->setEnabled(true);
		$this->em->persist($entity);
		$this->em->flush();
	}

	public function getLoginHistory($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->findLoginHistory($id);
	}
	
	public function setSidebarCollapsed($request)
	{
		$sidebarCollapsed = $request->request->get('sidebarCollapsed') == 'expanded' ? false : true;
		$user = $this->securityContext->getToken()->getUser();
		
		$user->setIsSidebarCollapsed($sidebarCollapsed);
		
		$em = $this->em;
		$em->persist($user);
		$em->flush();
		
		return new JsonResponse($sidebarCollapsed, 200);
	}
}