<?php

namespace CI\CoreBundle\Entity;

use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="user") 
 * @ORM\Entity(repositoryClass="CI\CoreBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="email", message="Sorry, this email address is already taken.")
 * @UniqueEntity(fields="username", message="Sorry, this username is already taken.")
 * @Gedmo\Loggable
 * @Assert\Callback(methods={"process"}, groups={"ValidateRole"})
 */
class User extends BaseUser
{
	const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
	const ROLE_ADMIN = 'ROLE_ADMIN';
	const ROLE_IT_ACCOUNT = 'ROLE_IT_ACCOUNT';
	const ROLE_RSM = 'ROLE_RSM';
	const ROLE_CDM = 'ROLE_CDM';
	const ROLE_TL = 'ROLE_TL';
	const ROLE_DISER = 'ROLE_DISER';
	
	const ACCESS_DEACTIVATE = 'deactivate';
	const ACCESS_REACTIVATE = 'reactivate';
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string", length=100)
	 * @Assert\NotBlank()
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	private $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	private $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gender", type="string", length=1)
	 * @Assert\NotBlank()
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	private $gender;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="birth_date", type="date", nullable=true)
	 * @Assert\Date()
	 * @Gedmo\Versioned
	 */
	private $birthDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_number", type="text", length=25, nullable=true)
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	private $contactNumber;
	
	/**
	 * @Assert\Email()
	 * @Assert\NotBlank(message="Please enter an email.")
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	protected $email;
	
	/**
	 * @Assert\NotBlank(message="Please enter a username.")
	 * @Assert\Type(type="string")
	 * @Gedmo\Versioned
	 */
	protected $username;
	
	/**
	 * @Assert\NotBlank(message="Please enter a password.", groups={"CustomRegistration"})
	 * @Assert\Type(type="string")
	 */
	protected $plainPassword;
	
	/**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="created_by", type="string", length=255)
     * @Gedmo\Blameable(on="create")
     */
    protected $createdBy;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     * @Gedmo\Blameable(on="update")
     */
    protected $updatedBy;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_sidebar_collapsed", type="boolean")
     */
    private $isSidebarCollapsed;
    
    /**
     * @ORM\OneToMany(targetEntity="UserLog", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userLogs;
    
       /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Store", mappedBy="adm", cascade={"persist", "remove"})
     */
    private $admStores;
    
    /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Store", mappedBy="rsm", cascade={"persist", "remove"})
     */
    private $rsmStores;
    
    /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Store", mappedBy="cdm", cascade={"persist", "remove"})
     */
    private $cdmStores;
    
    /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Store", mappedBy="tl", cascade={"persist", "remove"})
     */
    private $tlStores;
    
    /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Store", mappedBy="diser", cascade={"persist", "remove"})
     */
    private $diserStores;
    
    /**
     * @ORM\OneToMany(targetEntity="CI\InventoryBundle\Entity\Device", mappedBy="personInCharge", cascade={"persist", "remove"})
     */
    private $devices;
    
    private $formRole;
    
	public function __construct()
	{
		parent::__construct();
		$this->userLogs = new ArrayCollection();
		$this->stores = new ArrayCollection();
		$this->devices = new ArrayCollection();
		$this->isSidebarCollapsed = false;
		$this->rsmStores = new ArrayCollection();
		$this->cdmStores = new ArrayCollection();
		$this->tlStores = new ArrayCollection();
		$this->diserStores = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * Get firstName
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * Get lastName
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set gender
	 *
	 * @param string $gender
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * Get gender
	 *
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Set birthDate
	 *
	 * @param date $birthDate
	 */
	public function setBirthDate($birthDate)
	{
		$this->birthDate = $birthDate;
	}

	/**
	 * Get birthDate
	 *
	 * @return date
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

    /**
     * Set contactNumber
     *
     * @param text $contactNumber
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;
    }

    /**
     * Get contactNumber
     *
     * @return text 
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }
    
	/**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt = null)
    {
    	$this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt = null)
    {   
    	$this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
    	$this->createdBy = $createdBy;
    
    	return $this;
    }
    
    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
    	return $this->createdBy;
    }
    
    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     * @return User
     */
    public function setUpdatedBy($updatedBy)
    {
    	$this->updatedBy = $updatedBy;
    
    	return $this;
    }
    
    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
    	return $this->updatedBy;
    }
    
    /**
     * Get userLogs
     * 
     * @return ArrayCollection
     */
    public function getUserLogs()
    {
    	return $this->userLogs;
    }

    /**
     * Add userLogs
     *
     * @param \CI\CoreBundle\Entity\UserLog $userLogs
     * @return User
     */
    public function addUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
    {
        $this->userLogs[] = $userLogs;
    
        return $this;
    }

    /**
     * Remove userLogs
     *
     * @param \CI\CoreBundle\Entity\UserLog $userLogs
     */
    public function removeUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
    {
        $this->userLogs->removeElement($userLogs);
    }
    
    /**
     * Set isSidebarCollapsed
     *
     * @param boolean $isSidebarCollapsed
     * @return User
     */
    public function setIsSidebarCollapsed($isSidebarCollapsed)
    {
    	$this->isSidebarCollapsed = $isSidebarCollapsed;
    
    	return $this;
    }
    
    /**
     * Get isSidebarCollapsed
     *
     * @return boolean
     */
    public function getIsSidebarCollapsed()
    {
    	return $this->isSidebarCollapsed;
    }
    
    /**
     * Add RsmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $rsmStores
     * @return User
     */
    public function addRsmStore(\CI\InventoryBundle\Entity\Store $rsmStores)
    {
    	$this->rsmStores[] = $rsmStores;
    
    	return $this;
    }
    
    /**
     * Remove RsmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $rsmStores
     */
    public function removeRsmStore(\CI\InventoryBundle\Entity\Store $rsmStores)
    {
    	$this->rsmStores->removeElement($rsmStores);
    }
    
    /**
     * Get RsmStores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRsmStores()
    {
    	return $this->rsmStores;
    }
    
    
       /**
     * Add RsmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $rsmStores
     * @return User
     */
    public function addAdmStore(\CI\InventoryBundle\Entity\Store $admStores)
    {
    	$this->admStores[] = $admStores;
    
    	return $this;
    }
    
    /**
     * Remove RsmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $rsmStores
     */
    public function removeadmStore(\CI\InventoryBundle\Entity\Store $admStores)
    {
    	$this->admStores->removeElement($admStores);
    }
    
    /**
     * Get RsmStores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdmStores()
    {
    	return $this->admStores;
    }
    /**
     * Add cdmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $cdmStores
     * @return User
     */
    public function addCdmStore(\CI\InventoryBundle\Entity\Store $cdmStores)
    {
    	$this->cdmStores[] = $cdmStores;
    
    	return $this;
    }
    
    /**
     * Remove cdmStores
     *
     * @param \CI\InventoryBundle\Entity\Store $cdmStores
     */
    public function removeCdmStore(\CI\InventoryBundle\Entity\Store $cdmStores)
    {
    	$this->cdmStores->removeElement($cdmStores);
    }
    
    /**
     * Get cdmStores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCdmStores()
    {
    	return $this->cdmStores;
    }
    
    /**
     * Add tlStores
     *
     * @param \CI\InventoryBundle\Entity\Store $tlStores
     * @return User
     */
    public function addTlStore(\CI\InventoryBundle\Entity\Store $tlStores)
    {
    	$this->tlStores[] = $tlStores;
    
    	return $this;
    }
    
    /**
     * Remove tlStores
     *
     * @param \CI\InventoryBundle\Entity\Store $tlStores
     */
    public function removeTlStore(\CI\InventoryBundle\Entity\Store $tlStores)
    {
    	$this->tlStores->removeElement($tlStores);
    }
    
    /**
     * Get tlStores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTlStores()
    {
    	return $this->tlStores;
    }
    
    /**
     * Add diserStores
     *
     * @param \CI\InventoryBundle\Entity\Store $diserStores
     * @return User
     */
    public function addDiserStore(\CI\InventoryBundle\Entity\Store $diserStores)
    {
    	$this->diserStores[] = $diserStores;
    
    	return $this;
    }
    
    /**
     * Remove diserStores
     *
     * @param \CI\InventoryBundle\Entity\Store $diserStores
     */
    public function removeDiserStore(\CI\InventoryBundle\Entity\Store $diserStores)
    {
    	$this->diserStores->removeElement($diserStores);
    }
    
    /**
     * Get diserStores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiserStores()
    {
    	return $this->diserStores;
    }
    
    public function getStores()
    {
    	if ($this->hasRole(self::ROLE_DISER)) {
    		return $this->getDiserStores();
    	} else if ($this->hasRole(self::ROLE_TL)) {
    		return $this->getTlStores();
    	} else if ($this->hasRole(self::ROLE_CDM)) {
    		return $this->getCdmStores();
    	} else if ($this->hasRole(self::ROLE_RSM)) {
    		return $this->getRsmStores();
    	} else if ($this->hasRole(self::ROLE_ADMIN)) {
    		return $this->getAdmStores();
    	} else {
    		return array();
    	}
    }
    
    /**
     * Add devices
     *
     * @param \CI\InventoryBundle\Entity\Device $devices
     * @return User
     */
    public function addDevice(\CI\InventoryBundle\Entity\Device $devices)
    {
    	$this->devices[] = $devices;
    
    	return $this;
    }
    
    /**
     * Remove devices
     *
     * @param \CI\InventoryBundle\Entity\Device $devices
     */
    public function removeDevice(\CI\InventoryBundle\Entity\Device $devices)
    {
    	$this->devices->removeElement($devices);
    }
    
    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
    	return $this->devices;
    }
    
    public function setFormRole($role)
    {
    	$this->formRole = $role;
    	
    	return $this;
    }
    
    public function getFormRole()
    {
    	return $this->formRole;
    }
    
    public function isDeletable()
    {
    	if ($this->getStores()->count() > 0 || $this->getDevices()->count() > 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    public function getName()
    {
    	return $this->getLastName() ? $this->getFirstName() . ' ' . $this->getLastName() : $this->getFirstName();
    }
    
    private function translateRole($role)
    {
    	switch ($role) {
    		case self::ROLE_SUPER_ADMIN: return "SA";
    		case self::ROLE_ADMIN: return "Admin";
    		case self::ROLE_IT_ACCOUNT: return "IT";
    		case self::ROLE_RSM: return "RSM";
    		case self::ROLE_CDM: return "CDM";
    		case self::ROLE_TL: return "TL";
    		case self::ROLE_DISER: return "Diser";
    	}
    }
    
    public static function getSupervisorRoles($role)
    {
    	if ($role == User::ROLE_DISER) {
    		return array(User::ROLE_RSM, User::ROLE_CDM, User::ROLE_TL);
    	} elseif ($role == User::ROLE_TL) {
    		return array(User::ROLE_RSM, User::ROLE_CDM);
    	} elseif ($role == User::ROLE_CDM) {
    		return array(User::ROLE_RSM);
    	} else {
    		return array();
    	}
    }
    
    public function getRole()
    {
    	foreach ($this->getRoles() as $role) {
    		if ($role != self::ROLE_SUPER_ADMIN && $role != self::ROLE_DEFAULT) {
    			return $role;
    		}
    	}
    
    	return null;
    }
    
    public function getRoleLabel()
    {
    	$role = $this->getRole();
    	return $role === null ? $this->getName() : $this->getName() . ' (' . $this->translateRole($role) . ")";
    }
    
    public function process(ExecutionContextInterface $context)
    {	
    	$this->setRoles(array());
    	$this->addRole($this->getFormRole());
    }
}