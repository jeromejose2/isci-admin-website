<?php

namespace CI\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CICoreBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
