<?php

namespace CI\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Welcome controller
 *
 */
class WelcomeController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function indexAction()
    {
    	/* 
    	 * Redirect the user to the login page if user is not yet authenticated (not logged in) and 
    	 * redirect the user to the dashboard if authenticated (logged in). 
    	 */
    	if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
    		return $this->redirect($this->generateUrl('fos_user_security_login'));
    	} else {
    		return $this->redirect($this->generateUrl('dashboard'));
    	}
    }
}