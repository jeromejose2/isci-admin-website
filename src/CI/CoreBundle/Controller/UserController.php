<?php

namespace CI\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use CI\CoreBundle\Model\UserModel as Model;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.user.model');
	}
	
	/**
	 * Lists all User entities.
	 *
	 * @Route("/", name="user")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		 
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView()
		);
	}
	
	/**
     * Finds and displays a User entity.
     *
     * @Route("/{id}/show", name="user_show")
     * @Template()
     * @PreAuthorize("hasRole('ROLE_ADMIN')")
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    		
    	return array(
    		'user' => $entity
    	);
    }
	
	/**
	 * Displays a form to edit an existing User entity.
	 *
	 * @Route("/{id}/edit", name="user_edit")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function editAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getType($entity);
		$isSuper = $model->checkSuperRole($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity, $isSuper);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
					return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
		);
	}
	
	/**
	 * Deactivates an existing user entity.
	 *
	 * @Route("/{id}/access/{access}", name="user_manage_access")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function manageAccessAction($id, $access)
	{
		$model = $this->getModel();
		$model->manageAccess($id, $access);
		
		$this->get('session')->getFlashBag()->add('success', $model->getMessages($access));
		return $this->redirect($this->generateUrl('user'));
	}
	
	/**
	 * @Route("/{id}/login-history", name="user_login_history")
	 * @Template("CICoreBundle:User:loginHistory.html.twig")
	 * @PreAuthorize("hasRole('ROLE_ADMIN')")
	 */
	public function getLoginHistoryAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$log = $model->getLoginHistory($id);
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$log,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'user' => $entity,
			'pagination' => $pagination
		);
	}
	
	/**
	 * @Route("/sidebar-collapsed", name="user_sidebar_collapsed")
	 * @Method("POST")
	 * @PreAuthorize("isFullyAuthenticated()")
	 */
	public function setSidebarCollapsedAction(Request $request)
	{
		$model = $this->getModel();
		return $model->setSidebarCollapsed($request);
	}
	
	/**
	 * @Route("/{id}/log", name="user_log")
	 * @Method("get")
	 * @Template("CICoreBundle:Misc:userAuditLogs.html.twig")
	 */
	public function logAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('GedmoLoggable:LogEntry');
		$entity = $em->getRepository('CICoreBundle:User')->find($id);
		return array(
			'entity' => $entity,
			'logs' => $repo->getLogEntries($entity),
		);
	}
	
	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="user_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
		}
			
		return array(
				'entity' => $entity,
				'delete_form' => $deleteForm->createView(),
				'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * Deletes a User entity.
	 *
	 * @Route("/{id}", name="user_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
	
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
		}
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('delete'));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
	
		return $this->redirect($this->generateUrl('user'));
	}
	
	public function createDeleteForm($id)
	{
		return $this->createFormBuilder(array('id' => $id))
			->add('id', 'hidden')
			->setMethod('DELETE')
			->getForm();
	}
}