<?php
namespace CI\CoreBundle\Twig;

use Twig_Extension;
use Twig_Filter_Method;

class DecimalFilterExtension extends Twig_Extension
{
	/**
	 * {@inheritDoc}
	 */
	public function getFilters()
	{
		return array(
			'decimal' => new Twig_Filter_Method(
				$this,
				'decimalFormat'
			)
		);
	}
	
	public function decimalFormat($number, $decimals = null, $decPoint = '.', $thousandsSep = ',')
	{
		if (is_null($decimals)) {
			$parts = explode(".", $number);
			$inherentDecimals = count($parts) == 2 ? strlen($parts[1]) : 0;
			if (count($parts) == 2 && (int) $parts[1] !== 0) {
				$inherentDecimals = strlen($parts[1]);
			} else {
				$inherentDecimals = 0;
			}
		} else {
			$inherentDecimals = $decimals;
		}
		
		return number_format($number, $inherentDecimals, $decPoint, $thousandsSep);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getName()
	{
		return 'ci_decimal_filter';
	}
}