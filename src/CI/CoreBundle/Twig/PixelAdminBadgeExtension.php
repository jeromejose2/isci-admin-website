<?php

namespace CI\CoreBundle\Twig;

use Twig_Extension;
use Twig_Function_Method;

class PixelAdminBadgeExtension extends Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        $options = array('pre_escape' => 'html', 'is_safe' => array('html'));

        return array(
            'badge'          => new Twig_Function_Method($this, 'badgeFunction', $options),
            'badge_primary'  => new Twig_Function_Method($this, 'badgePrimaryFunction', $options),
            'badge_success'  => new Twig_Function_Method($this, 'badgeSuccessFunction', $options),
            'badge_info'     => new Twig_Function_Method($this, 'badgeInfoFunction', $options),
            'badge_warning'  => new Twig_Function_Method($this, 'badgeWarningFunction', $options),
            'badge_danger'   => new Twig_Function_Method($this, 'badgeDangerFunction', $options)
        );
    }

    /**
     * Returns the HTML code for a badge.
     *
     * @param string $text The text of the badge
     * @param string $type The type of badge
     *
     * @return string The HTML code of the badge
     */
    public function badgeFunction($text, $type = 'default')
    {
        return sprintf('<span class="badge%s">%s</span>', ($type ? ' badge-' . $type : ''), $text);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function badgePrimaryFunction($text)
    {
        return $this->badgeFunction($text, 'primary');
    }

    /**
     * Returns the HTML code for a success badge.
     *
     * @param string $text The text of the badge
     *
     * @return string The HTML code of the badge
     */
    public function badgeSuccessFunction($text)
    {
        return $this->badgeFunction($text, 'success');
    }

    /**
     * Returns the HTML code for a warning badge.
     *
     * @param string $text The text of the badge
     *
     * @return string The HTML code of the badge
     */
    public function badgeWarningFunction($text)
    {
        return $this->badgeFunction($text, 'warning');
    }

    /**
     * Returns the HTML code for a important badge.
     *
     * @param string $text The text of the badge
     *
     * @return string The HTML code of the badge
     */
    public function badgeDangerFunction($text)
    {
        return $this->badgeFunction($text, 'danger');
    }

    /**
     * Returns the HTML code for a info badge.
     *
     * @param string $text The text of the badge
     *
     * @return string The HTML code of the badge
     */
    public function badgeInfoFunction($text)
    {
        return $this->badgeFunction($text, 'info');
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'ci_pixel_admin_badge';
    }
}