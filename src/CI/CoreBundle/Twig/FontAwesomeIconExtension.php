<?php
namespace CI\CoreBundle\Twig;

use Twig_Extension;
use Twig_Function_Method;

class FontAwesomeIconExtension extends Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return array(
            'FAIcon' => new Twig_Function_Method(
                $this,
                'iconFunction',
                array('pre_escape' => 'html', 'is_safe' => array('html'))
            ),
        	'FAListIcon' => new Twig_Function_Method(
        		$this,
        		'listIconFunction',
        		array('pre_escape' => 'html', 'is_safe' => array('html'))
        	)
        );
    }

    /**
     * Returns the HTML code for the given icon.
     *
     * @param string $icon  The name of the icon
     *
     * @return string The HTML code for the icon
     */
    public function iconFunction($icon, $class = null)
    {
        return sprintf('<span class="%sfa fa-%s"></span>', is_null($class) ? "" : $class . " " , $icon);
    }
    
    /**
     * Returns the HTML code for the given icon for lists.
     *
     * @param string $icon  The name of the icon
     *
     * @return string The HTML code for the icon
     */
    public function listIconFunction($icon, $class = null)
    {
    	return sprintf('<span class="%sfa fa-fw fa-%s"></span>', is_null($class) ? "" : $class . " " , $icon);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'ci_font_awesome_icon';
    }
}