<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Store
 *
 * @ORM\Table(name="store")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\StoreRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="Branch already exists.")
 */
class Store extends BaseEntity
{
        const KEY_ACCOUNT = 1;
	const OSA_TRACKED = 2;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="text")
	 * @Assert\NotBlank(message="Address must not be blank.")
	 * @Assert\Type(type="string")
	 */
	private $address;
	
	/**
	 * @ORM\ManyToOne(targetEntity="City", inversedBy="stores")
	 * @Assert\NotBlank(message="City must not be blank.")
	 */
	private $city;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_details", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $contactDetails;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Region", inversedBy="stores")
	 * @Assert\NotBlank(message="Region must not be blank.")
	 */
	private $region;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Distributor", inversedBy="stores")
	 * @Assert\NotBlank(message="Distributor must not be blank.")
	 */
	private $distributor;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Chain", inversedBy="stores")
	 * @Assert\NotBlank(message="Chain must not be blank.")
	 */
	private $chain;
	
	/**
	 * @ORM\ManyToOne(targetEntity="StoreType", inversedBy="stores")
	 * @Assert\NotBlank(message="Branch type must not be blank.")
	 */
	private $storeType;
        
        /**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="admStores")
	 * @Assert\NotBlank(message="ADM must not be blank.")
	 */
	private $adm;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="rsmStores")
	 * @Assert\NotBlank(message="RSM must not be blank.")
	 */
	private $rsm;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="cdmStores")
	 */
	private $cdm;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="tlStores")
	 */
	private $tl;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="diserStores")
	 * @Assert\NotBlank(message="Merchandiser must not be blank.")
	 */
	private $diser;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Product", mappedBy="stores", cascade={"persist"})
	 */
	protected $products;
	
	/**
	 * @ORM\OneToMany(targetEntity="InventoryReport", mappedBy="store", cascade={"persist"})
	 */
	protected $inventoryReports;
	
        /**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_osa_tracked", type="boolean")
	 */
	private $isOsaTracked = false;
        
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->products = new ArrayCollection();
		$this->inventoryReports = new ArrayCollection();
                $this->isOsaTracked = false;
	}

    /**
     * Set name
     *
     * @param string $name
     * @return Store
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Store
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set city
     *
     * @param \CI\InventoryBundle\Entity\City $city
     * @return Store
     */
    public function setCity(\CI\InventoryBundle\Entity\City $city = null)
    {
    	$this->city = $city;
    
    	return $this;
    }
    
    /**
     * Get city
     *
     * @return \CI\InventoryBundle\Entity\City
     */
    public function getCity()
    {
    	return $this->city;
    }
    
    /**
     * Set contactDetails
     *
     * @param string $contactDetails
     * @return Store
     */
    public function setContactDetails($contactDetails)
    {
    	$this->contactDetails = $contactDetails;
    
    	return $this;
    }
    
    /**
     * Get contactDetails
     *
     * @return string
     */
    public function getContactDetails()
    {
    	return $this->contactDetails;
    }
    
    /**
     * Set region
     *
     * @param \CI\InventoryBundle\Entity\Region $region
     * @return Store
     */
    public function setRegion(\CI\InventoryBundle\Entity\Region $region = null)
    {
    	$this->region = $region;
    
    	return $this;
    }
    
    /**
	 * Set isOsaTracked
	 *
	 * @param boolean $isOsaTracked
	 * @return Product
	 */
	public function setIsOsaTracked($isOsaTracked)
	{
		$this->isOsaTracked = $isOsaTracked;
	
		return $this;
	}
        /**
	 * Get isOsaTracked
	 *
	 * @return boolean
	 */
	public function getIsOsaTracked()
	{
		return $this->isOsaTracked;
	}
    
    /**
     * Get region
     *
     * @return \CI\InventoryBundle\Entity\Region
     */
    public function getRegion()
    {
    	return $this->region;
    }
    
    /**
     * Set distributor
     *
     * @param \CI\InventoryBundle\Entity\Distributor $distributor
     * @return Store
     */
    public function setDistributor(\CI\InventoryBundle\Entity\Distributor $distributor = null)
    {
    	$this->distributor = $distributor;
    
    	return $this;
    }
    
    /**
     * Get distributor
     *
     * @return \CI\InventoryBundle\Entity\Distributor
     */
    public function getDistributor()
    {
    	return $this->distributor;
    }
    
    /**
     * Set chain
     *
     * @param \CI\InventoryBundle\Entity\Chain $chain
     * @return Store
     */
    public function setChain(\CI\InventoryBundle\Entity\Chain $chain = null)
    {
    	$this->chain = $chain;
    
    	return $this;
    }
    
    /**
     * Get chain
     *
     * @return \CI\InventoryBundle\Entity\Chain
     */
    public function getChain()
    {
    	return $this->chain;
    }
    
    /**
     * Set storeType
     *
     * @param \CI\InventoryBundle\Entity\StoreType $storeType
     * @return Store
     */
    public function setStoreType(\CI\InventoryBundle\Entity\StoreType $storeType = null)
    {
    	$this->storeType = $storeType;
    
    	return $this;
    }
    
    /**
     * Get storeType
     *
     * @return \CI\InventoryBundle\Entity\StoreType
     */
    public function getStoreType()
    {
    	return $this->storeType;
    }
    
    /**
     * Set rsm
     *
     * @param \CI\CoreBundle\Entity\User $rsm
     * @return Store
     */
    public function setRsm(\CI\CoreBundle\Entity\User $rsm = null)
    {
    	$this->rsm = $rsm;
    
    	return $this;
    }
    
    /**
     * Get rsm
     *
     * @return \CI\CoreBundle\Entity\User
     */
    public function getRsm()
    {
    	return $this->rsm;
    }
    
     /**
     * Set rsm
     *
     * @param \CI\CoreBundle\Entity\User $rsm
     * @return Store
     */
    public function setAdm(\CI\CoreBundle\Entity\User $adm = null)
    {
    	$this->adm = $adm;
    
    	return $this;
    }
    
    /**
     * Get rsm
     *
     * @return \CI\CoreBundle\Entity\User
     */
    public function getAdm()
    {
    	return $this->adm;
    }
    
    /**
     * Set cdm
     *
     * @param \CI\CoreBundle\Entity\User $cdm
     * @return Store
     */
    public function setCdm(\CI\CoreBundle\Entity\User $cdm = null)
    {
    	$this->cdm = $cdm;
    
    	return $this;
    }
    
    /**
     * Get cdm
     *
     * @return \CI\CoreBundle\Entity\User
     */
    public function getCdm()
    {
    	return $this->cdm;
    }
    
    /**
     * Set tl
     *
     * @param \CI\CoreBundle\Entity\User $tl
     * @return Store
     */
    public function setTl(\CI\CoreBundle\Entity\User $tl = null)
    {
    	$this->tl = $tl;
    
    	return $this;
    }
    
    /**
     * Get tl
     *
     * @return \CI\CoreBundle\Entity\User
     */
    public function getTl()
    {
    	return $this->tl;
    }
    
    /**
     * Set diser
     *
     * @param \CI\CoreBundle\Entity\User $diser
     * @return Store
     */
    public function setDiser(\CI\CoreBundle\Entity\User $diser = null)
    {
    	$this->diser = $diser;
    
    	return $this;
    }
    
    /**
     * Get diser
     *
     * @return \CI\CoreBundle\Entity\User
     */
    public function getDiser()
    {
    	return $this->diser;
    }
    
    /**
     * Add products
     *
     * @param \CI\InventoryBundle\Entity\Product $products
     * @return Store
     */
    public function addProduct(\CI\InventoryBundle\Entity\Product $products)
    {
    	$this->products[] = $products;
    }
    
    /**
     * Remove products
     *
     * @param \CI\InventoryBundle\Entity\Product $products
     */
    public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
    {
    	$this->products->removeElement($products);
    }
    
    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
    	return $this->products;
    }

    /**
     * Add inventoryReports
     *
     * @param \CI\InventoryBundle\Entity\InventoryReport $inventoryReports
     * @return Store
     */
    public function addInventoryReport(\CI\InventoryBundle\Entity\InventoryReport $inventoryReports)
    {
    	$this->inventoryReports[] = $inventoryReports;
    
    	return $this;
    }
    
    /**
     * Remove inventoryReports
     *
     * @param \CI\InventoryBundle\Entity\InventoryReport $inventoryReports
     */
    public function removeInventoryReport(\CI\InventoryBundle\Entity\InventoryReport $inventoryReports)
    {
    	$this->inventoryReports->removeElement($inventoryReports);
    }
    
    /**
     * Get inventoryReports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryReports()
    {
    	return $this->inventoryReports;
    }
    
    public function isDeletable()
    {
    	if ($this->getInventoryReports()->count() > 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    public function getLog()
    {
    	return array(	
    		'Name' => $this->getName(),
    		'Address' => $this->getAddress(),
    		'City' => $this->getCity()->getName(),
    		'Region' => $this->getRegion()->getName(),
    		'Distributor' => $this->getDistributor()->getName(),
    		'Chain' => $this->getChain()->getName(),
    		'Branch Type' => $this->getStoreType()->getName(),
                'OSA Tracked' => $this->getIsOsaTracked() ? 'Yes' : 'No'
    	);
    }
    
    public function getDisplayClass()
    {
    	return "Branch";
    }
}