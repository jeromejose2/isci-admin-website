<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\ExecutionContextInterface;
use CI\InventoryBundle\Helper\ApiUploadedFile;

/**
 * InventoryReport
 *
 * @ORM\Table(name="um_report")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\UmReportRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validate"})
 */
class UmReport extends BaseEntity
{
	/**
	 * @var date
	 *
	 * @ORM\Column(name="report_date", type="date")
	 * @Assert\NotBlank(message="Report date is required.")
	 * @Assert\Date(message="Report date is not a valid date.")
	 */
	private $reportDate;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="report_creation_date", type="date")
	 * @Assert\NotBlank(message="Report creation date is required.")
	 * @Assert\Date(message="Report creation date is not a valid date.")
	 */
	private $reportCreationDate;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="report_last_updated_date", type="date", nullable=true)
	 * @Assert\Date(message="Report last updated date is not a valid date.")
	 */
	private $reportLastUpdatedDate;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="note", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $note;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="150M")
	 */
	private $file;
	
	private $temp;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Device", inversedBy="inventoryReports")
	 * @Assert\NotBlank(message="Device is required.")
	 */
	private $device;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Store", inversedBy="inventoryReports")
	 * @Assert\NotBlank(message="Store is required.")
	 */
	private $store;
	
	/**
	 * @ORM\OneToMany(targetEntity="UmReportItem", mappedBy="inventoryReport", cascade={"persist", "remove"})
	 * @Assert\Count(min="1", minMessage="At least one product is required.")
	 * @Assert\Valid
	 */
	private $items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
    
    /**
     * Set path
     *
     * @param string $path
     * @return InventoryReport
     */
    public function setPath($path)
    {
    	$this->path = $path;
    
    	return $this;
    }
    
    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
    	return $this->path;
    }
    
    /**
     * Set note
     *
     * @param string $note
     * @return InventoryReport
     */
    public function setNote($note)
    {
    	$this->note = $note;
    
    	return $this;
    }
    
    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
    	return $this->note;
    }
    
    /**
     * Set reportDate
     *
     * @param \DateTime $reportDate
     * @return InventoryReport
     */
    public function setReportDate($reportDate)
    {
        $this->reportDate = $reportDate;
    
        return $this;
    }

    /**
     * Get reportDate
     *
     * @return \DateTime 
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }
    
    /**
     * Set reportCreationDate
     *
     * @param \DateTime $reportCreationDate
     * @return InventoryReport
     */
    public function setReportCreationDate($reportCreationDate)
    {
    	$this->reportCreationDate = $reportCreationDate;
    
    	return $this;
    }
    
    /**
     * Get reportCreationDate
     *
     * @return \DateTime
     */
    public function getReportCreationDate()
    {
    	return $this->reportCreationDate;
    }
    
    /**
     * Set reportLastUpdatedDate
     *
     * @param \DateTime $reportLastUpdatedDate
     * @return InventoryReport
     */
    public function setReportLastUpdatedDate($reportLastUpdatedDate)
    {
    	$this->reportLastUpdatedDate = $reportLastUpdatedDate;
    
    	return $this;
    }
    
    /**
     * Get reportLastUpdatedDate
     *
     * @return \DateTime
     */
    public function getReportLastUpdatedDate()
    {
    	return $this->reportLastUpdatedDate;
    }
    
    /**
     * Set device
     *
     * @param \CI\InventoryBundle\Entity\Device $device
     * @return InventoryReport
     */
    public function setDevice(\CI\InventoryBundle\Entity\Device $device = null)
    {
    	$this->device = $device;
    
    	return $this;
    }
    
    /**
     * Get device
     *
     * @return \CI\InventoryBundle\Entity\Device
     */
    public function getDevice()
    {
    	return $this->device;
    }

    /**
     * Set store
     *
     * @param \CI\InventoryBundle\Entity\Store $store
     * @return InventoryReport
     */
    public function setStore(\CI\InventoryBundle\Entity\Store $store = null)
    {
        $this->store = $store;
    
        return $this;
    }

    /**
     * Get store
     *
     * @return \CI\InventoryBundle\Entity\Store 
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Add items
     *
     * @param \CI\InventoryBundle\Entity\UmReportItem $items
     * @return InventoryReport
     */
    public function addItem(\CI\InventoryBundle\Entity\UmReportItem $items)
    {
    	$items->setInventoryReport($this);
        $this->items[] = $items;
    
        return $this;
    }

    /**
     * Remove items
     *
     * @param \CI\InventoryBundle\Entity\UmReportItem $items
     */
    public function removeItem(\CI\InventoryBundle\Entity\UmReportItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }
    
    /**
     * Sets file.
     *
     * @param ApiUploadedFile $file
     */
    public function setFile(ApiUploadedFile $file = null)
    {
    	$this->file = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath())) {
    		// store the old name to delete after the update
    		$this->temp = $this->getAbsolutePath();
    	} else {
    		$this->path = 'initial';
    	}
    }
    
    /**
     * Get file.
     *
     * @return ApiUploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    public function getAbsolutePath()
    {
    	return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }
    
    public function getWebPath()
    {
    	return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }
    
    protected function getUploadRootDir()
    {
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	return 'uploads/inventory-report/' . $this->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if (null !== $this->getFile()) {
    		// do whatever you want to generate a unique name
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->path = $filename . '.' . $this->getFile()->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	 
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	 
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		unlink($this->getUploadRootDir().'/'.$this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	$this->file = $this->getUploadRootDir().'/'.$this->path;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function validate(ExecutionContextInterface $context)
    {
    	if (!$this->getReportCreationDate()) {
    		$context->addViolationAt('reportCreationDate', 'Report creation date is required.');
    	}
    
    	if (!$this->getCreatedBy()) {
    		$context->addViolationAt('createdBy', 'Username is required.');
    	}
    	
    	if ($this->file) {
    		if (!file_exists($this->file->getPathname())) {
    			$context->addViolationAt('file', 'Image not found.');
    		}
    	
    		if (!in_array($this->file->guessExtension(), array('jpg', 'png', 'jpeg'))) {
    			$context->addViolationAt('file', 'Only .jpg, .png and .jpeg images are allowed.');
    		}
    	}
    }
    
    public function getLog()
    {
    	return array(
    		'Device' => $this->getDevice()->getName(),
    		'Store' => $this->getStore()->getName(),
    		'Report Date' => $this->getReportDate(),
    		'Note' => $this->getNote(),
    		'Report Creation Date' => $this->getReportCreationDate(),
    		'Report Last Updated Date' => $this->getReportLastUpdatedDate()
    	);
    }
}