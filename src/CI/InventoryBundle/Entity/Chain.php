<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Chain
 *
 * @ORM\Table(name="chain")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ChainRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validate"})
 * @UniqueEntity(fields="name", message="Chain already exists.")
 */
class Chain extends BaseEntity
{
	const KEY_ACCOUNT = 1;
	const OSA_TRACKED = 2;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_parent", type="boolean")
	 */
	private $isParent = true;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_key_account", type="boolean")
	 */
	private $isKeyAccount;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_osa_tracked", type="boolean")
	 */
	private $isOsaTracked;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Chain", inversedBy="children")
	 */
	private $parent;
	
	/**
	 * @ORM\OneToMany(targetEntity="Chain", mappedBy="parent", cascade={"persist", "remove"})
	 * @Assert\Valid
	 */
	private $children;
	
	/**
	 * @ORM\OneToMany(targetEntity="Store", mappedBy="chain", cascade={"persist", "remove"})
	 */
	private $stores;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->stores = new ArrayCollection();
		$this->isKeyAccount = false;
		$this->isOsaTracked = false;
	}
	
	/**
	 * Set isParent
	 *
	 * @param boolean $isParent
	 * @return Store
	 */
	public function setIsParent($isParent)
	{
		$this->isParent = $isParent;
	
		return $this;
	}
	
	/**
	 * Get isParent
	 *
	 * @return boolean
	 */
	public function getIsParent()
	{
		return $this->isParent;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Chain
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set isKeyAccount
	 *
	 * @param boolean $isKeyAccount
	 * @return Chain
	 */
	public function setIsKeyAccount($isKeyAccount)
	{
		$this->isKeyAccount = $isKeyAccount;
	
		return $this;
	}
	
	/**
	 * Get isKeyAccount
	 *
	 * @return boolean
	 */
	public function getIsKeyAccount()
	{
		return $this->isKeyAccount;
	}
	
	/**
	 * Set isOsaTracked
	 *
	 * @param boolean $isOsaTracked
	 * @return Chain
	 */
	public function setIsOsaTracked($isOsaTracked)
	{
		$this->isOsaTracked = $isOsaTracked;
	
		return $this;
	}
	
	/**
	 * Get isOsaTracked
	 *
	 * @return boolean
	 */
	public function getIsOsaTracked()
	{
		return $this->isOsaTracked;
	}
	
	/**
	 * Add stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 * @return Chain
	 */
	public function addStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$this->stores[] = $stores;
	
		return $this;
	}
	
	/**
	 * Remove stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 */
	public function removeStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$this->stores->removeElement($stores);
	}
	
	/**
	 * Get stores
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getStores()
	{
		return $this->stores;
	}
	
	/**
	 * Set parent
	 *
	 * @param \CI\InventoryBundle\Entity\Chain $parent
	 * @return Store
	 */
	public function setParent(\CI\InventoryBundle\Entity\Chain $parent = null)
	{
		$this->parent = $parent;
	
		return $this;
	}
	
	/**
	 * Get parent
	 *
	 * @return \CI\InventoryBundle\Entity\Chain
	 */
	public function getParent()
	{
		return $this->parent;
	}
	
	/**
	 * Add children
	 *
	 * @param \CI\InventoryBundle\Entity\Chain $children
	 * @return Store
	 */
	public function addChildren(\CI\InventoryBundle\Entity\Chain $children)
	{
		$this->children[] = $children;
	
		return $this;
	}
	
	/**
	 * Remove children
	 *
	 * @param \CI\InventoryBundle\Entity\Chain $children
	 */
	public function removeChildren(\CI\InventoryBundle\Entity\Chain $children)
	{
		$this->children->removeElement($children);
	}
	
	/**
	 * Get children
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildren()
	{
		return $this->children;
	}
    
    public function isDeletable()
    {
    	if ($this->getChildren()->count() > 0 || $this->getStores()->count() > 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    public function getParentName()
    {
    	return $this->getIsParent() ? null : $this->getParent()->getName();
    }
    
    public function validate(ExecutionContextInterface $context)
    {
    	if ($this->getIsParent() && $this->getParent()) {
    		$context->addViolationAt('isParent', 'Parent chains cannot have parents.');
    	}
    
    	if (!$this->getIsParent() && !$this->getParent()) {
    		$context->addViolationAt('parent', 'Please choose a parent chain.');
    	}
    }
    
    public function getLog()
    {
    	return array(
    		'Parent Chain' => $this->getIsParent() ? 'Yes' : 'No',
    		'Parent' => $this->getParent() ? $this->getParent()->getName() : '',
    		'Active' => $this->getActive() ? 'Yes' : 'No',
    		'Name' => $this->getName(),
    		'Key Account' => $this->getIsKeyAccount() ? 'Yes' : 'No',
    		'OSA Tracked' => $this->getIsOsaTracked() ? 'Yes' : 'No'
    	);
    }
}