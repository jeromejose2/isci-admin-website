<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Device
 *
 * @ORM\Table(name="device")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\DeviceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Device extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text")
	 * @Assert\NotBlank(message="Description must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $description;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="device_id", type="string", length=255)
	 * @Assert\NotBlank(message="Device ID must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $deviceId;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CI\CoreBundle\Entity\User", inversedBy="devices")
	 * @Assert\NotBlank(message="Person in-charge must not be blank.")
	 */
	private $personInCharge;
	
	/**
	 * @ORM\OneToMany(targetEntity="InventoryReport", mappedBy="device", cascade={"persist"})
	 */
	protected $inventoryReports;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->inventoryReports = new ArrayCollection();
	}

    /**
     * Set name
     *
     * @param string $name
     * @return Device
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Device
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deviceId
     *
     * @param string $deviceId
     * @return Device
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    
        return $this;
    }

    /**
     * Get deviceId
     *
     * @return string 
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set personInCharge
     *
     * @param \CI\CoreBundle\Entity\User $personInCharge
     * @return Device
     */
    public function setPersonInCharge(\CI\CoreBundle\Entity\User $personInCharge = null)
    {
        $this->personInCharge = $personInCharge;
    
        return $this;
    }

    /**
     * Get personInCharge
     *
     * @return \CI\CoreBundle\Entity\User 
     */
    public function getPersonInCharge()
    {
        return $this->personInCharge;
    }
    
    /**
     * Add inventoryReports
     *
     * @param \CI\InventoryBundle\Entity\InventoryReport $inventoryReports
     * @return Device
     */
    public function addInventoryReport(\CI\InventoryBundle\Entity\InventoryReport $inventoryReports)
    {
    	$this->inventoryReports[] = $inventoryReports;
    
    	return $this;
    }
    
    /**
     * Remove inventoryReports
     *
     * @param \CI\InventoryBundle\Entity\InventoryReport $inventoryReports
     */
    public function removeInventoryReport(\CI\InventoryBundle\Entity\InventoryReport $inventoryReports)
    {
    	$this->inventoryReports->removeElement($inventoryReports);
    }
    
    /**
     * Get inventoryReports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryReports()
    {
    	return $this->inventoryReports;
    }
    
    public function isDeletable()
    {
    	if ($this->getInventoryReports()->count() > 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    public function getLog()
    {
    	return array(
    		'Status' => $this->getActive() ? 'Enabled' : 'Disabled',
    		'Name' => $this->getName(),
    		'Description' => $this->getDescription(),
    		'Device ID' => $this->getDeviceId(),
    		'Diser' => $this->getPersonInCharge()->getName()
    	);
    }
}