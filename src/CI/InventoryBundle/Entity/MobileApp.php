<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * MobileApp
 *
 * @ORM\Table(name="mobile_app")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class MobileApp extends BaseEntity
{
	/**
	 * @ORM\Column(name="app_id", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter the app ID.")
	 */
	protected $appId;
	
	/**
	 * @ORM\Column(name="app_secret", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter the app secret.")
	 */
	protected $appSecret;
	
	/**
	 * @ORM\Column(name="app_version", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter the app version.")
	 */
	protected $appVersion;
	
	/**
	 * @ORM\Column(name="data_version", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter the data version.")
	 */
	protected $dataVersion;
	
	/**
	 * @ORM\Column(name="pin", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter the PIN.")
	 */
	protected $pin;
	
	/**
	 * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$fileName;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="150M")
	 */
	private $file;
	
	private $temp;
	
	public function getAbsolutePath()
	{
		return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
	}
	
	public function getWebPath()
	{
		return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/mobile-app/';
	}

    /**
     * Set appId
     *
     * @param string $appId
     * @return MobileApp
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    
        return $this;
    }

    /**
     * Get appId
     *
     * @return string 
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * Set appSecret
     *
     * @param string $appSecret
     * @return MobileApp
     */
    public function setAppSecret($appSecret)
    {
        $this->appSecret = $appSecret;
    
        return $this;
    }

    /**
     * Get appSecret
     *
     * @return string 
     */
    public function getAppSecret()
    {
        return $this->appSecret;
    }

    /**
     * Set appVersion
     *
     * @param string $appVersion
     * @return MobileApp
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
    
        return $this;
    }

    /**
     * Get appVersion
     *
     * @return string 
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * Set dataVersion
     *
     * @param string $dataVersion
     * @return MobileApp
     */
    public function setDataVersion($dataVersion)
    {
        $this->dataVersion = $dataVersion;
    
        return $this;
    }

    /**
     * Get dataVersion
     *
     * @return string 
     */
    public function getDataVersion()
    {
        return $this->dataVersion;
    }
    
    /**
     * Set pin
     *
     * @param string $pin
     * @return MobileApp
     */
    public function setPin($pin)
    {
    	$this->pin = $pin;
    
    	return $this;
    }
    
    /**
     * Get pin
     *
     * @return string
     */
    public function getPin()
    {
    	return $this->pin;
    }
    
    /**
     * Set fileName
     *
     * @param \file_name $fileName
     * @return MobileApp
     */
    public function setFileName($fileName)
    {
    	$this->fileName = $fileName;
    
    	return $this;
    }
    
    /**
     * Get fileName
     *
     * @return \file_name
     */
    public function getFileName()
    {
    	return $this->fileName;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return MobileApp
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
    	$this->file = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath())) {
    		// store the old name to delete after the update
    		$this->temp = $this->getAbsolutePath();
    	} else {
    		$this->path = 'initial';
    	}
    }
    
    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if (null !== $this->getFile()) {
    		// do whatever you want to generate a unique name
    		$this->setFileName($this->getFile()->getClientOriginalName());
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->path = $filename . '.' . $this->getFile()->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		unlink($this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	
    	$this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function getName()
    {
    	return 'Mobile App Settings';
    }
    
    // for the event subscriber to change the class of the object
    public function getDisplayClass()
    {
    	return " ";
    }
    
    public function getLog()
    {
    	return array(
    		'App ID' => $this->getAppId(),
    		'App Secret' => $this->getAppSecret(),
    		'App Version' => $this->getAppVersion(),
    		'Data Version' => $this->getAppVersion(),
    		'PIN' => $this->getPin(),
    		'App' => $this->getFileName()
    	);
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	if (!empty($this->file)) {
    		$this->setUpdatedAt(new \DateTime('now'));
    	}
    }
}