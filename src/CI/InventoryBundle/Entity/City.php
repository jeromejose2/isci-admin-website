<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\CityRepository")
 */
class City
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
	 * @ORM\OneToMany(targetEntity="Store", mappedBy="city")
	 */
	private $stores;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->stores = new ArrayCollection();
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add stores
     *
     * @param \CI\InventoryBundle\Entity\Store $stores
     * @return City
     */
    public function addStore(\CI\InventoryBundle\Entity\Store $stores)
    {
        $this->stores[] = $stores;
    
        return $this;
    }

    /**
     * Remove stores
     *
     * @param \CI\InventoryBundle\Entity\Store $stores
     */
    public function removeStore(\CI\InventoryBundle\Entity\Store $stores)
    {
        $this->stores->removeElement($stores);
    }

    /**
     * Get stores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStores()
    {
        return $this->stores;
    }
}