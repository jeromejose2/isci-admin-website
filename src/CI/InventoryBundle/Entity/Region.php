<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\RegionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="Region already exists.")
 */
class Region extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="Name must not be blank.")
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @ORM\OneToMany(targetEntity="Store", mappedBy="region", cascade={"persist", "remove"})
	 */
	private $stores;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->stores = new ArrayCollection();
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Region
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Add stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 * @return Region
	 */
	public function addStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$this->stores[] = $stores;
	
		return $this;
	}
	
	/**
	 * Remove stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 */
	public function removeStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$this->stores->removeElement($stores);
	}
	
	/**
	 * Get stores
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getStores()
	{
		return $this->stores;
	}
    
    public function isDeletable()
    {
    	if ($this->getStores()->count() > 0) {
    		return false;
    	}
    	
    	return true;
    }
    
    public function getLog()
    {
    	return array(
    		'Active' => $this->getActive() ? 'Yes' : 'No',
    		'Name' => $this->getName(),
    	);
    }
}