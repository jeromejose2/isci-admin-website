<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * ActivityLog
 *
 * @ORM\Table(name="activity_log")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ActivityLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ActivityLog
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="timestamp", type="datetime")
	 */
	private $timestamp;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="action", type="string", length=6)
	 * @Assert\Type(type="string")
	 * @Assert\Choice(choices = {"create", "update", "delete"})
	 */
	private $action;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="user", type="string", length=255)
	 * @Assert\Type(type="string")
	 */
	private $user;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="class", type="string", length=255)
	 * @Assert\Type(type="string")
	 */
	private $class;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\Type(type="string")
	 */
	private $name;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="route_name", type="string", length=255)
	 * @Assert\Type(type="string")
	 */
	private $routeName;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="entity_id", type="integer")
	 * @Assert\Type(type="integer")
	 */
	private $entityId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return EntityAudit
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return EntityAudit
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }
    
    /**
     * Set user
     *
     * @param string $user
     * @return EntityAudit
     */
    public function setUser($user)
    {
    	$this->user = $user;
    
    	return $this;
    }
    
    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
    	return $this->user;
    }
    
    /**
     * Set class
     *
     * @param string $class
     * @return ActivityLog
     */
    public function setClass($class)
    {
    	$this->class = $class;
    
    	return $this;
    }
    
    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
    	return $this->class;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EntityAudit
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set routeName
     *
     * @param string $routeName
     * @return EntityAudit
     */
    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
    
        return $this;
    }

    /**
     * Get routeName
     *
     * @return string 
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     * @return EntityAudit
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    
        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }
    
    public function getClassDisplay()
    {
    	return strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $this->getClass()));
    }
}