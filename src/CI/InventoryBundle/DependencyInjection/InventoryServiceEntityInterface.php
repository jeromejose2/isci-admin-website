<?php

namespace CI\InventoryBundle\DependencyInjection;

/**
 * InventoryServiceEntityInterface should be implemented by entity classes that depend on the InventoryService.
 */
interface InventoryServiceEntityInterface
{
	public function getISItems();
}