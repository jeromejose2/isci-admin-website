<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\StoreType;
use CI\InventoryBundle\Form\Type\StoreTypeType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class StoreTypeModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new StoreType();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new StoreTypeType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('branch type'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Branch Type has been created successfully.';
			case 'update': 
				return 'Branch Type has been updated successfully.';
			case 'delete': 
				return 'Branch Type has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(StoreType $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This branch type has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'storetype_delete',
			'return_path' => 'storetype_show',
			'name' => '[Branch Type] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'StoreType'
		);
	
		$options = array(
			'route' => 'storetype',
			'name' => 'Branch Type',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'storetype',
			'name' => 'Branch Type',
			'classes' => array(
				'CI\InventoryBundle\Entity\StoreType'
			)
		);
	}
}