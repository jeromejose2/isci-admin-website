<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Region;
use CI\InventoryBundle\Form\Type\RegionType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class RegionModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new Region();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new RegionType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('region'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Region has been created successfully.';
			case 'update': 
				return 'Region has been updated successfully.';
			case 'delete': 
				return 'Region has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Region $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This region has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'region_delete',
			'return_path' => 'region_show',
			'name' => '[Region] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Region'
		);
	
		$options = array(
			'route' => 'region',
			'name' => 'Region',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'region',
			'name' => 'Region',
			'classes' => array(
				'CI\InventoryBundle\Entity\Region'
			)
		);
	}
}