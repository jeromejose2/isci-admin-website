<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Device;
use CI\InventoryBundle\Form\Type\DeviceType;
use CI\InventoryBundle\Form\Type\SearchFilterType;
use Symfony\Component\Form\Form;
use Doctrine\Common\Collections\ArrayCollection;

class DeviceModel extends BaseEntityModel
{
	private $originalStores;
	
	public function getNewEntity()
	{
		return new Device();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new DeviceType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('device'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Device has been created successfully.';
			case 'update': 
				return 'Device has been updated successfully.';
			case 'delete': 
				return 'Device has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
	
		if ($entity->getId()) {
		}
	
		$em->persist($entity);
		
		$settings = $em->getRepository('CIInventoryBundle:MobileApp')->find(1);
		$settings->setDataVersion($settings->getDataVersion() + 1);
		$em->persist($settings);
		$em->flush();
	}
	
	public function isDeletable(Device $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This device has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'device_delete',
			'return_path' => 'device_show',
			'name' => '[Device] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Device'
		);
	
		$options = array(
			'route' => 'device',
			'name' => 'Device',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function confirmChangeStatus($entity, $status, $target_route, $return_route)
	{
		$name = '[Device] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
		
		return array(
			'status' => $status,
			'name' => $name,
			'target_route' => $target_route,
			'return_route' => $return_route,
			'change_status_form' => $this->createChangeStatusForm(array(
				'id' => $entity->getId(),
				'status' => $status
			))->createView(),
		);
	}
	
	public function createChangeStatusForm($params = array())
	{
		return $this->getFormFactory()->createBuilder('form', $params)
			->setMethod('PUT')
			->add('status', 'hidden')
			->add('id', 'hidden')
			->getForm();
	}
	
	public function changeStatus($newStatus, $entity)
	{
		$setActive = $newStatus === Device::STATUS_INACTIVE ? false : true;
		if ($setActive !== $entity->isActive()) {
			$entity->setActive($setActive);
			$em = $this->getEM();
			$em->persist($entity);
			$em->flush();
				
			return $entity->getActive() ? 'Enabled' : 'Disabled';
		} else {
			throw new \Exception(sprintf('This device is already %s', $entity->getActive() ? 'Enabled' : 'Disabled'));
		}
	}
	
	public function getLog()
	{
		return array(
			'route' => 'device',
			'name' => 'Device',
			'classes' => array(
				'CI\InventoryBundle\Entity\Device',
				'CI\InventoryBundle\Entity\DeviceStore'
			)
		);
	}
}