<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\Form\Form;
use CI\InventoryBundle\Entity\Store;
use CI\InventoryBundle\Form\Type\StoreType;
use CI\InventoryBundle\Form\Type\StoreFilterType;

class StoreModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new Store();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new StoreType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new StoreFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Branch has been created successfully.';
			case 'update': 
				return 'Branch has been updated successfully.';
			case 'delete': 
				return 'Branch has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Store $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This store has already been added to a report and therefore can no longer be deleted.');
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($form->get('save')->isClicked()) {
			
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		
		$settings = $em->getRepository('CIInventoryBundle:MobileApp')->find(1);
		$settings->setDataVersion($settings->getDataVersion() + 1);
		$em->persist($settings);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'store_delete',
			'return_path' => 'store_show',
			'name' => '[Branch] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Store'
		);
	
		$options = array(
			'route' => 'store',
			'name' => 'Branch',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'store',
			'name' => 'Branch',
			'classes' => array(
				'CI\InventoryBundle\Entity\Store'
			)
		);
	}
	
	public function getBranchReportFilter($regionId, $chainId)
	{
		$sc = $this->getSecurityContext();
		return $this->getRepository()->getBranchReportFilter($regionId, $chainId, $sc);
	}
}