<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseEntityModel extends BaseEmptyEntityModel
{
	abstract public function getNewEntity();
	
	public function findExistingEntity($id)
	{
		return $this->getRepository()->find($id);
	}
	
	abstract public function getFormType($entity = null);
	
	abstract public function getFilterFormType();
	
	abstract public function getMessages($action);
	
	public function getIndex($params = null)
	{
		return $this->getRepository()->findAll($params);
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		if (!is_null($entity) && is_object($entity)) {
			$em->persist($entity);
			$em->flush();
		}
	}
	
	abstract public function getDeleteParams($entity);
	
	public function deleteEntity($id)
	{
		$em = $this->getEM();
	
		$entity = $this->findExistingEntity($id);
	
		$em->remove($entity);
		$em->flush();
	}
	
	abstract public function getRevision($id);
	
	abstract public function getLog();
}