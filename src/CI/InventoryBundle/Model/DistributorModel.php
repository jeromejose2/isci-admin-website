<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Distributor;
use CI\InventoryBundle\Form\Type\DistributorType;
use CI\InventoryBundle\Form\Type\SearchFilterType;

class DistributorModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new Distributor();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new DistributorType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new SearchFilterType('distributor'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Distributor has been created successfully.';
			case 'update': 
				return 'Distributor has been updated successfully.';
			case 'delete': 
				return 'Distributor has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Distributor $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This distributor has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'distributor_delete',
			'return_path' => 'distributor_show',
			'name' => '[Distributor] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Distributor'
		);
	
		$options = array(
			'route' => 'distributor',
			'name' => 'Distributor',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'distributor',
			'name' => 'Distributor',
			'classes' => array(
				'CI\InventoryBundle\Entity\Distributor'
			)
		);
	}
}