<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Form;

use CI\InventoryBundle\Entity\MobileApp;
use CI\InventoryBundle\Form\Type\MobileAppType;

class MobileAppModel extends BaseEmptyEntityModel
{
	public function findExistingEntity()
	{
		return $this->getRepository()->find(1);
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new MobileAppType(), $entity, array('method' => 'PUT'));
	}
	
	public function saveEntity(Form $form, MobileApp $entity)
	{
		$em = $this->getEM();
	
		if ($form->get('save')->isClicked()) {
				
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
	
		$em->persist($entity);
		$em->flush();
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'MobileApp'
		);
	
		$options = array(
			'route' => 'mobileapp',
			'name' => 'Mobile Application Settings',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'mobileapp',
			'name' => 'Mobile Application Settings',
			'classes' => array(
				'CI\InventoryBundle\Entity\MobileApp'
			)
		);
	}
        
        public function samplesendmail()
	{
  
//         $message = \Swift_Message::newInstance()
//        ->setSubject('Hello Email')
//        ->setFrom('jerome.jose@nuworks.ph')
//        ->setTo('mesekarome@gmail.com')
//        ->setBody('bodyyyyy'
//            $this->renderView(
//                // app/Resources/views/Emails/registration.html.twig
//                'Emails/sendemailreport.html.twig',
//                array('name' => 'Jerome Jose')
//            ),
//            'text/html'
//        )
//    ;
           $message = '<html><body>';
			$message .= '<img src="http://css-tricks.com/examples/WebsiteChangeRequestForm/images/wcrf-header.png" alt="Website Change Request" />';
			$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td> </td></tr>";
			$message .= "<tr><td><strong>Email:</strong> </td><td> </td></tr>";
			$message .= "<tr><td><strong>Type of Change:</strong> </td><td> </td></tr>";
			$message .= "<tr><td><strong>Urgency:</strong> </td><td> </td></tr>";
			$message .= "<tr><td><strong>URL To Change (main):</strong> </td><td></td></tr>";
			$message .= "<tr><td><strong>NEW Content:</strong> </td><td> </td></tr>";
			$message .= "</table>";
			$message .= "</body></html>";
                        
                        
		
            
            $to = 'jerome.jose@nuworks.ph';
			
			$subject = 'Jerome Test Email';
			
			$headers = "From: " . $to . "\r\n";
			$headers .= "Reply-To: ". strip_tags('jerome.jose@nuworks.ph') . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                        
 if (mail($to, $subject, $message, $headers)) {
             return 1;
            } else {
             return 2;
            }

    
    
	}
}