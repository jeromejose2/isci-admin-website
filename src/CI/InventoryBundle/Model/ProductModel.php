<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\Product as EntityType;
use CI\InventoryBundle\Form\Type\ProductType as EntityFormType;
use CI\InventoryBundle\Form\Type\ProductFilterType as FilterFormType;

class ProductModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	public function getNewEntity()
	{
		return new EntityType();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Product created successfully.';
			case self::ACTION_UPDATE: return 'Product updated successfully.';
			case self::ACTION_DELETE: return 'Product has been deleted.';
			case self::ACTION_STATUS_CHANGE : return 'Product status has been updated to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
 		if (!$entity->isDeletable()) {
 			throw new \Exception('This store has already been added to a report and therefore can no longer be deleted.');
 		}
	}
	
	public function isEditable(EntityType $entity)
	{
	}
	
	public function getIndex($params = null)
	{
		return $this->getRepository()->findAll($params);
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($form->get('save')->isClicked()) {
			
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		
		$settings = $em->getRepository('CIInventoryBundle:MobileApp')->find(1);
		$settings->setDataVersion($settings->getDataVersion() + 1);
		$em->persist($settings);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'product_delete',
    		'return_path' => 'product_show',
    		'name' => '[Product] ' . $entity->getName()
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Product'
		);
	
		$options = array(
			'route' => 'product',
			'name' => 'Product',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'product',
			'name' => 'Product',
			'classes' => array(
				'CI\InventoryBundle\Entity\Product'
			)
		);
	}
	
	public function getBranchReportFilter($branchId)
	{
		return $this->getRepository()->getBranchReportFilter($branchId);
	}
}