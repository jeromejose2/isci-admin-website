<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use PHPExcel;
use PHPExcel_IOFactory;

use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Form\Type\SummaryOSAReportFilterType;
use CI\InventoryBundle\Form\Type\UmReportFilterType;
use CI\InventoryBundle\Form\Type\BranchOSAReportFilterType;
use CI\InventoryBundle\Form\Type\DownloadOsaFilterType;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;

class StoreReportModel extends BaseEmptyEntityModel
{
	public function getSummaryOSAReportFilterType()
	{
		return $this->getFormFactory()->create(new SummaryOSAReportFilterType($this->getSecurityContext()));
	}
        public function getUmReportFilterType()
        {
            return $this->getFormFactory()->create(new UmReportFilterType($this->getSecurityContext()));
        }
	
	public function getBranchOSAReportFilterType()
	{
		return $this->getFormFactory()->create(new BranchOSAReportFilterType($this->getSecurityContext()));
	}
	
	private function formatSheetTitle($title)
	{
		return substr(preg_replace('~[\\\\/\[\]*?\':]~', '_', $title), 0, 31);
	}
	
	private function prepareParams(array &$params)
	{
		$sc = $this->getSecurityContext();
		$user = $sc->getToken()->getUser();
		if (!$sc->isGranted(User::ROLE_ADMIN)) {
			if ($sc->isGranted(User::ROLE_RSM)) {
				$params['rsm'] = $user;
			} else if ($sc->isGranted(User::ROLE_CDM)) {
				$params['cdm'] = $user;
			} else if ($sc->isGranted(User::ROLE_TL)) {
				$params['tl'] = $user;
			} else if ($sc->isGranted(User::ROLE_DISER)) {
				$params['diser'] = $user;
			}
		}
	}
	
	public function getSummaryOSAReportData(array $params)
	{
		$this->prepareParams($params);
		$products = array();
		$productList = array();
                
		$stores = $this->getRepository('CIInventoryBundle:Store')->getSummaryOSAReport($params);
               
		$results = $this->getRepository('CIInventoryBundle:InventoryReport')->getSummaryOSA($params);
		
		array_map(function($e) use (&$products, &$productList) {
			$products[$e['storeId']][$e['productId']] = $e['percentage'];
			if (!array_key_exists($e['productId'], $productList)) {
				$productList[$e['productId']]['name'] = $e['productName'];
				$productList[$e['productId']]['isCore'] = $e['isCore'];
				$productList[$e['productId']]['isNpd'] = $e['isNpd'];
				$productList[$e['productId']]['isSeasonal'] = $e['isSeasonal'];
			}
		}, $results);
		
		return array('label' => $productList, 'stores' => $stores, 'products' => $products);
	}
        
        public function getUmReportData(array $params)
	{
		$this->prepareParams($params);
		$products = array();
		$productList = array();
                
		$stores = $this->getRepository('CIInventoryBundle:Store')->getSummaryOSAReport($params);
               
		$results = $this->getRepository('CIInventoryBundle:InventoryReport')->getSummaryOSA($params);
		
		array_map(function($e) use (&$products, &$productList) {
			$products[$e['storeId']][$e['productId']] = $e['percentage'];
			if (!array_key_exists($e['productId'], $productList)) {
				$productList[$e['productId']]['name'] = $e['productName'];
				$productList[$e['productId']]['isCore'] = $e['isCore'];
				$productList[$e['productId']]['isNpd'] = $e['isNpd'];
				$productList[$e['productId']]['isSeasonal'] = $e['isSeasonal'];
			}
		}, $results);
		
		return array('label' => $productList, 'stores' => $stores, 'products' => $products);
	}
	
	public function exportSummaryOSAReport(array $params, $isDownload = false, $xls = null)
	{
		$data = $this->getSummaryOSAReportData($params);

		$rightStyle = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$borderThin = array('style' => \PHPExcel_Style_Border::BORDER_THIN);
		$bordersStyle = array('borders' => array('allborders' => $borderThin));
		$bodyBorderStyle = array('borders' => array('left' => $borderThin, 'right' => $borderThin));
		
		if (!$isDownload) {
			$objPHPExcel = new \PHPExcel();
			$objPHPExcel->getProperties()->setCreator('ISCI')->setTitle('Summary Report');
			$xls = $objPHPExcel->setActiveSheetIndex(0);
			$xls->setTitle('Summary Report');
		}
		
		//headers
		$xls->setCellValue('A1', 'ACCOUNT'); $xls->getColumnDimension('A')->setAutoSize(true);
		$xls->setCellValue('B1', 'BRANCH'); $xls->getColumnDimension('B')->setAutoSize(true);
		$xls->setCellValue('C1', 'REGION'); $xls->getColumnDimension('C')->setAutoSize(true);
		$xls->setCellValue('D1', 'STORE TYPE'); $xls->getColumnDimension('D')->setAutoSize(true);
		$xls->getStyle('A1:D1')->applyFromArray(array(
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            	'color' => array('rgb' => '000000')
        	),
			'font' => array('color' => array('rgb' => 'FFFFFF')),
			'alignment' => array(
				'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		
		$letter = $lastLetter = 'E';
		foreach ($data['label'] as $label) {
			$xls->setCellValue($letter . '1', $label['name']);
			$xls->getColumnDimension($letter)->setWidth(10);
			if ($label['isCore']) {
				$xls->getStyle($letter . '1')->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
		            	'color' => array('rgb' => 'BFEFFF')
		        	),
				));
			} else if ($label['isNpd']) {
				$xls->getStyle($letter . '1')->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'FFE4E1')
					),
				));
			} else if ($label['isSeasonal']) {
				$xls->getStyle($letter . '1')->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'FAFAD2')
					),
				));
			}
			$lastLetter = $letter;
			$letter++;
		}
		
		$xls->getStyle('A1:' . $lastLetter . '1')->applyFromArray($bordersStyle);
		
		//table data
		$counter = $lastRow = 2;
		$storeCount = count($data['stores']);
		foreach ($data['stores'] as $key => $store) {
			$currentBorder = $bodyBorderStyle;
			
			if ($key + 1 == $storeCount) {
				$currentBorder['borders']['bottom'] = $borderThin;
			}
			
			$xls->setCellValue('A' . $counter, $store->getChain()->getName());
			$xls->getStyle('A' . $counter)->applyFromArray($currentBorder);
			$xls->setCellValue('B' . $counter, $store->getName());
			$xls->getStyle('B' . $counter)->applyFromArray($currentBorder);
			$xls->setCellValue('C' . $counter, $store->getRegion()->getName());
			$xls->getStyle('C' . $counter)->applyFromArray($currentBorder);
			$xls->setCellValue('D' . $counter, $store->getStoreType()->getName());
			$xls->getStyle('D' . $counter)->applyFromArray($currentBorder);
			
			$letter = $lastLetter = 'E';
			foreach ($data['label'] as $key => $label) {
				$percent = isset($data['products'][$store->getId()][$key]) ? $data['products'][$store->getId()][$key] : 0;
				$xls->setCellValue($letter . $counter, $percent . '%', true);
				if ($percent < 100) {
					$xls->getStyle($letter . $counter)->applyFromArray(array(
						'fill' => array(
							'type' => \PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'FFE4E1')
						),
						'font' => array('color' => array('rgb' => 'FF0000')),
					));
				}
				$xls->getStyle($letter . $counter)->applyFromArray($currentBorder);
				$lastLetter = $letter;
				$letter++;
			}
			$lastRow = $counter;
			$counter++;
		}
		$xls->getStyle('E2:' . $lastLetter . $lastRow)->applyFromArray($rightStyle);
		
		if (!$isDownload) {
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$filename = 'Summary_Report_' . date('M-d-Y');
			return array('objWriter' => $objWriter, 'filename' => $filename);
		} else {
			return $xls;
		}
	}
	
	public function getBranchOSAReportData($params)
	{
		$products = $reportList = array();
		$results = $this->getRepository('CIInventoryBundle:InventoryReport')->getBranchOSA($params);
		array_map(function($e) use (&$products, &$reportList) {
			if (!array_key_exists($e['productId'], $products)) {
				$products[$e['productId']]['categoryName'] = $e['categoryName'];
				$products[$e['productId']]['productName'] = $e['productName'];
				$products[$e['productId']]['isCore'] = $e['isCore'];
				$products[$e['productId']]['isNpd'] = $e['isNpd'];
				$products[$e['productId']]['isSeasonal'] = $e['isSeasonal'];
				$products[$e['productId']]['daysVisited'] = 1;
				$products[$e['productId']]['okCount'] = $e['available'] ? 1 : 0;
				$products[$e['productId']]['reports'][$e['reportId']] = $e['available'];
			} else {
				$products[$e['productId']]['daysVisited']++;
				if ($e['available']) {
					$products[$e['productId']]['okCount']++;
				}
				$products[$e['productId']]['reports'][$e['reportId']] = $e['available'];
			}
			
			if (!array_key_exists($e['reportId'], $reportList)) {
				$reportList[$e['reportId']] = $e['reportDate'];
			}
		}, $results);
		
		return array('label' => $reportList, 'products' => $products); 
	}
	
	public function exportBranchOSAReport($params, $isDownload = false, $xls = null)
	{
		$data = $this->getBranchOSAReportData($params);
	
		$boldStyle = array('bold' => true);
		$rightStyle = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
		$centerStyle = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$borderThin = array('style' => \PHPExcel_Style_Border::BORDER_THIN);
		$bordersStyle = array('borders' => array('allborders' => $borderThin));
		$bodyBorderStyle = array('borders' => array('left' => $borderThin, 'right' => $borderThin));
	
		if (!$isDownload) {
			$objPHPExcel = new \PHPExcel();
			$objPHPExcel->getProperties()->setCreator('ISCI')->setTitle('Branch Report');
			$xls = $objPHPExcel->setActiveSheetIndex(0);
			$xls->setTitle($this->formatSheetTitle($params['store']->getName()));
		}
	
		//headers
		$xls->setCellValue('A1', 'DISER: ' . $params['store']->getDiser()->getName());
		$xls->getStyle('A1')->applyFromArray(array('font' => $boldStyle));
		
		$xls->setCellValue('A3', 'CAT'); $xls->getColumnDimension('A')->setWidth(15);
		$xls->setCellValue('B3', 'ITEM DESCRIPTION'); $xls->getColumnDimension('B')->setWidth(25);
		$xls->setCellValue('C3', 'TOTAL'); $xls->getColumnDimension('C')->setWidth(7);
		$xls->setCellValue('D3', 'OK'); $xls->getColumnDimension('D')->setWidth(7);
		$xls->setCellValue('E3', '%OK'); $xls->getColumnDimension('E')->setWidth(7);
		$xls->getStyle('A3:E3')->applyFromArray($centerStyle + array('font' => $boldStyle));
		$xls->getStyle('A3:B3')->applyFromArray(array(
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'BFEFFF')
			)
		));
	
		$letter = $lastLetter = 'F';
		foreach ($data['label'] as $report) {
			$reportDate = new \DateTime($report);
			$xls->setCellValue($letter . '3', $reportDate->format('n/j'));
			$xls->getColumnDimension($letter)->setWidth(7);
			$lastLetter = $letter;
			$letter++;
		}
	
		$xls->getStyle('A3:' . $lastLetter . '3')->applyFromArray($bordersStyle);
		$xls->getStyle('F3:' . $lastLetter . '3')->applyFromArray($centerStyle);
		$xls->getStyle('C3:' . $lastLetter . '3')->applyFromArray(array(
			'fill' => array(
				'type' => \PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'CFDBC5')
			)
		));
	
		//table data
		$counter = $lastRow = 4;
		$prdCount = count($data['products']);
		foreach ($data['products'] as $key => $product) {
			$currentBorder = $bodyBorderStyle;
	
			if ($key == $prdCount) {
				$currentBorder['borders']['bottom'] = $borderThin;
			}
	
			$xls->setCellValue('A' . $counter, $product['categoryName']);
			$xls->getStyle('A' . $counter)->applyFromArray($currentBorder);
			$xls->setCellValue('B' . $counter, $product['productName']);
			$xls->getStyle('B' . $counter)->applyFromArray($currentBorder);
				
			if ($product['isCore']) {
				$xls->getStyle('A' . $counter . ':B' .$counter)->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'BFEFFF')
					)
				));
			} else if ($product['isNpd']) {
				$xls->getStyle('A' . $counter . ':B' .$counter)->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'FFE4E1')
					)
				));
			} else if ($product['isSeasonal']) {
				$xls->getStyle('A' . $counter . ':B' .$counter)->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'FAFAD2')
					)
				));
			}
				
			$xls->setCellValue('C' . $counter, $product['daysVisited']);
			$xls->getStyle('C' . $counter)->applyFromArray($currentBorder);
			$xls->setCellValue('D' . $counter, $product['okCount'] == 0 ? '-' : $product['okCount']);
			$xls->getStyle('D' . $counter)->applyFromArray($currentBorder);
				
			try{
				$percentOK = round(($product['okCount'] / $product['daysVisited']) * 100);
			} catch (\Exception $e) {
				$percentOK = 0;
			}
				
			$xls->setCellValue('E' . $counter, $percentOK . "%");
			$xls->getStyle('E' . $counter)->applyFromArray($currentBorder);
				
			$xls->getStyle('C' . $counter . ':E' .$counter)->applyFromArray(array(
				'fill' => array(
					'type' => \PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'CFDBC5')
				)
			));
				
			if ($percentOK < 100) {
				$xls->getStyle('E' . $counter)->applyFromArray(array(
					'fill' => array(
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'FF0000')
					),
					'font' => array('color' => array('rgb' => 'FFFFFF')),
				));
			}
	
			$letter = $lastLetter = 'F';
			foreach ($data['label'] as $key => $report) {
				$isPresent = isset($product['reports'][$key]) ? $product['reports'][$key] : 'N/A';
				$xls->setCellValue($letter . $counter, $isPresent);
				if ($isPresent != true) {
					$xls->getStyle($letter . $counter)->applyFromArray(array(
						'fill' => array(
							'type' => \PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'FFE4E1')
						)
					));
				}
				$xls->getStyle($letter . $counter)->applyFromArray($currentBorder);
				$lastLetter = $letter;
				$letter++;
			}
			$lastRow = $counter;
			$counter++;
		}
	
		$xls->getStyle('C2:C' . $lastRow)->applyFromArray($centerStyle);
		$xls->getStyle('D2:' . $lastLetter . $lastRow)->applyFromArray($rightStyle);
	
		if (!$isDownload) {
			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$filename = 'Branch_Report_' . str_replace(' ', '_', $params['store']->getName()) . '_' . date('M-d-Y');
			return array('objWriter' => $objWriter, 'filename' => $filename);
		} else {
			return $xls;
		}
	}
	
	public function getDownloadOsa($type, array $params = null)
	{
		switch ($type) {
			case 'filter':
				return $this->getFormFactory()->create(new DownloadOsaFilterType($this->getSecurityContext()));
			case 'xls':
				$this->prepareParams($params);
				
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator('ISCI')->setTitle('Stock Availability Report');
				$xls = $objPHPExcel->setActiveSheetIndex(0);
				
				//summary sheet
				$objPHPExcel->getSheet(0)->setTitle('Summary Report');
				$xls = $this->exportSummaryOSAReport($params, true, $xls);
                                
                   	
                            
				//core average sheet
				$xls = $objPHPExcel->createSheet(1);
				$prModel = $this->get('ci.product.report.model');
				$objPHPExcel->getSheet(1)->setTitle('Core Average Report');
				$xls = $prModel->getCoreAverageReport('xls', $params, true, $xls);

				//frequency vs ok sheet
				$xls = $objPHPExcel->createSheet(2);
				$objPHPExcel->getSheet(2)->setTitle('Total Chain Average Report');
				$xls = $prModel->getFrequencyVsOkReport('xls', $params, true, $xls);
				
				//per store sheet
				$sheetCount = 3;
				$stores = $this->getRepository('CIInventoryBundle:Store')->getSummaryOSAReport($params);
				
				foreach ($stores as $store) {
					$params['store'] = $store;
					$xls = $objPHPExcel->createSheet($sheetCount);
					$objPHPExcel->getSheet($sheetCount)->setTitle($this->formatSheetTitle($store->getName()));
					$xls = $this->exportBranchOSAReport($params, true, $xls);
					$sheetCount++;
				}
				
				$objPHPExcel->setActiveSheetIndex(0);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$chainName = !empty($params['chain']) ? (str_replace(" ", "_", $params['chain']->getName()) . "_") : "";
				$filename = 'Stock_Availability_Report_' . $chainName . date('M-d-Y');
	
				return array('objWriter' => $objWriter, 'filename' => $filename);
		}
	}
}