<?php

namespace CI\InventoryBundle\Model;

use CI\InventoryBundle\Entity\Chain;
use CI\InventoryBundle\Form\Type\ChainType;
use CI\InventoryBundle\Form\Type\ChainFilterType;

class ChainModel extends BaseEntityModel
{
	public function getNewEntity()
	{
		return new Chain();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new ChainType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new ChainFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case 'create': 
				return 'New Chain has been created successfully.';
			case 'update': 
				return 'Chain has been updated successfully.';
			case 'delete': 
				return 'Chain has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Chain $entity)
	{
		if (!$entity->isDeletable()) {
			throw new \Exception('This chain has already been added to a transaction and therefore can no longer be deleted.');
		}
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'chain_delete',
			'return_path' => 'chain_show',
			'name' => '[Chain] ' . $entity->getName() . ' (ID #' . $entity->getId() . ')'
		);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Chain'
		);
	
		$options = array(
			'route' => 'chain',
			'name' => 'Chain',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'chain',
			'name' => 'Chain',
			'classes' => array(
				'CI\InventoryBundle\Entity\Chain'
			)
		);
	}
}