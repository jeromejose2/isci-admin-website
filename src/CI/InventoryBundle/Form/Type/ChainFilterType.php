<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChainFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6)
		))
		->add('parent', 'entity', array(
			'class' => 'CIInventoryBundle:Chain',
			'label' => 'Parent',
			'required' => false,
			'empty_value' => 'All parents',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(true);
			}
		))
                ->add('isOsaTracked', 'checkbox', array(
			'required' => false,
			'label' => 'OSA Tracked',
			'attr'=> array('align_with_widget' => true)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_chainfilter';
	}
}