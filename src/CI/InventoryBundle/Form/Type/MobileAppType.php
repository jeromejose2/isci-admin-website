<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MobileAppType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('appId', 'text', array(
			'label' => 'App ID'
		))
		->add('appSecret', 'text', array(
			'label' => 'App Secret'
		))
		->add('appVersion', 'text', array(
			'label' => 'App Version'
		))
		->add('dataVersion', 'text', array(
			'label' => 'Data Version'
		))
		->add('file', 'file', array(
// 			'constraints' => array(new NotBlank(array('message' => 'You must submit a video.'))),
			'label' => 'App',
			'attr' => array(
				'class' => 'custom-upload'
			)
		))
		->add('pin', 'text', array(
			'label' => 'PIN'
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\MobileApp'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_mobileapp';
	}
}