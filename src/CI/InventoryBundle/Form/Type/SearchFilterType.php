<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchFilterType extends AbstractType
{
    protected $placeholder;
    
    public function __construct($placeholder = null)
    {
        $this->placeholder = $placeholder;
    }
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $placeholder = $this->placeholder;
        
        $builder
            ->add('status', 'choice', array(
                'required' => false,
        		'empty_value' => 'All',
        		'data' => true,
        		'choices' => array(
        		    false => 'Inactive',
        			true => 'Active')))
        	->add('query', 'text', array(
        	    'label' => ucwords($placeholder),
        		'required' => false,
        		'attr' => array(
	        		'placeholder' => 'Search by ' . $placeholder . ' name',
        		)	
        	));
    }

    public function getName()
    {
        return 'ci_inventorybundle_searchfilter';
    }
}
