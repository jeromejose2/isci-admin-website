<?php

namespace CI\InventoryBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\InventoryBundle\Form\DataTransformer\CustomNumberTransformer;
 
class CustomNumberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$transformer = new CustomNumberTransformer();
        $builder->addModelTransformer($transformer);
    }
 
    public function getParent()
    {
        return 'text';
    }
 
    public function getName()
    {
        return 'custom_number';
    }
}
