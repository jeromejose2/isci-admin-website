<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use CI\CoreBundle\Entity\User;

class DeviceType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name')
			->add('description', 'textarea', array(
				'attr' => array(
					'rows' => 4
				)
			))
			->add('deviceId', 'text', array(
				'label' => 'Device ID',
				'attr' => array(
					'widget_col' => 6
				)
			))
			->add('personInCharge', 'entity', array(
				'class' => 'CICoreBundle:User',
				'label' => 'Assigned User',
				'empty_value' => 'Choose a Admin/RSM/CDM or TL/Diser',
				'property' => 'roleLabel',
				'attr' => array('class' => 'select2'),
				'query_builder' => function($er) {
					return $er->findAssignedUser();
				}
			))
			->add('save', 'submit', array(
				'attr' => array(
					'class' => 'btn btn-success submit-button',
					'data-loading-text' => "Saving..."
				)
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Device'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_device';
	}
}