<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use CI\CoreBundle\Entity\User;

class StoreType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'attr' => array('widget_col' => 6)
		))
		->add('address', 'textarea', array(
			'attr' => array('rows' => 4)
		))
		->add('rsm', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'RSM',
			'empty_value' => 'Choose a RSM',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_RSM);
			}
		))
		->add('cdm', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'CDM',
			'empty_value' => 'Choose a CDM',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_CDM);
			}
		))
		->add('tl', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'TL',
			'empty_value' => 'Choose a TL',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_TL);
			}
		))
		->add('diser', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'Diser',
			'empty_value' => 'Choose a diser',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_DISER);
			}
		))
		->add('city', 'entity', array(
			'class' => 'CIInventoryBundle:City',
			'empty_value' => 'Choose a city',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('contactDetails', 'textarea', array(
			'label' => 'Contact Details',
			'required' => false,
			'attr'=> array('rows' => 4)
		))
		->add('region', 'entity', array(
			'class' => 'CIInventoryBundle:Region',
			'empty_value' => 'Choose a region',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('distributor', 'entity', array(
			'class' => 'CIInventoryBundle:Distributor',
			'empty_value' => 'Choose a distributor',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('chain', 'entity', array(
			'class' => 'CIInventoryBundle:Chain',
			'empty_value' => 'Choose a chain',
			'property' => 'name',
			'group_by' => 'parentName',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('storeType', 'entity', array(
			'class' => 'CIInventoryBundle:StoreType',
			'empty_value' => 'Choose a branch type',
			'label' => 'Branch Type',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
                    ->add('isOsaTracked', 'checkbox', array(
                    'required' => false,
                    'label' => 'OSA Tracked',
                    'attr'=> array(
                            'class' => 'px',
                            'align_with_widget' => true
                    )
                ))
                        
                        ->add('adm', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'ADMIN',
			'empty_value' => 'Choose a ADM',
			'property' => 'name',
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_ADMIN);
			}
		))
                                      
                        
		->add('save', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Store'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_store';
	}
}