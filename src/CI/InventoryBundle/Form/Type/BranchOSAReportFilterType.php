<?php

namespace CI\InventoryBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints\NotBlank;
use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Store;
use CI\InventoryBundle\Entity\Chain;

class BranchOSAReportFilterType extends AbstractType
{
	protected $sc;
	
	public function __construct(SecurityContext $sc)
	{	
		$this->sc = $sc;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$sc = $this->sc;
    	$userId = $sc->getToken()->getUser()->getId();
    	
    	if ($sc->isGranted('ROLE_ADMIN')) {
    		$userId = null;
    	}
		
		$builder
		->setMethod('GET')
		->add('region', 'entity', array(
			'class' => 'CIInventoryBundle:Region',
			'property' => 'name',
			'label' => 'Region',
			'empty_value' => 'Choose a region',
			'attr' => array('class' => 'select2 region'),
//			'constraints' => array(new NotBlank(array('message' => 'Please enter a region.'))),
			'query_builder' => function($repository) use ($userId) {
				return $repository->findAllQb($userId);
			}
		))
		                      #jerome
                ->add('storeClassification', 'choice', array(
			'required' => false,
			'label' => 'Store Classification',
			'multiple' => true,
			'expanded' => true,
			'attr' => array('inline' => true),
			'choices' => array(
//				Store::KEY_ACCOUNT => 'Key Account',
				Store::OSA_TRACKED => 'Top Account'
			)
		))
//                ->add('chainClassification', 'choice', array(
//			'required' => false,
//			'label' => 'Chain Classification',
//			'multiple' => true,
//			'expanded' => true,
//			'attr' => array('inline' => true),
//			'choices' => array(
////				Chain::KEY_ACCOUNT => 'Key Account',
//				Chain::OSA_TRACKED => 'OSA Tracked'
//			)
//		))
            ->add('productClassification', 'choice', array(
        		'required' => false,
        		'label' => 'Product Classification',
        		'multiple' => true,
        		'expanded' => true,
        		'attr' => array('inline' => true),
        		'choices' => array(
        			Product::CORE => 'Core',
        			Product::NPD => 'NPD',
        			Product::SEASONAL => 'Seasonal',
        			Product::OSA_TRACKED => 'Promo Packs'//'OSA Tracked'
        		)
        	))
//                ->add('store', 'entity', array(
//        		'class' => 'CIInventoryBundle:Store',
//        		'property' => 'name',
//        		'label' => 'Branch',
//				'required' => false,
//	        	'empty_value' => 'All branches',
//        		'attr' => array('class' => 'select2'),
//        		'query_builder' => function($repository) use ($userId) {
//        			return $repository->findAllQb($userId);
//        		}
//        	))
                        #jerome
		->add('chain', 'entity', array(
			'class' => 'CIInventoryBundle:Chain',
			'property' => 'name',
			'label' => 'Chain',
			'empty_value' => 'Choose a chain',
			'attr' => array('class' => 'select2 chain'),
//			'constraints' => array(new NotBlank(array('message' => 'Please enter a chain.'))),
			'query_builder' => function($repository) use ($userId) {
				return $repository->findAllQb(false, $userId);
			}
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Date From',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => array(new NotBlank(array('message' => 'Please enter a date.'))),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Date To',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'constraints' => array(new NotBlank(array('message' => 'Please enter a date.'))),
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
		
		$renderStore = function (Form $form, $regionId = null, $chainId = null) use ($userId) {
			$form->add('store', 'entity', array(
				'class' => 'CIInventoryBundle:Store',
				'property' => 'name',
				'label' => 'Branch',
                                  'required' => false,
				'empty_value' => 'Choose a branch',
				'attr' => array('class' => 'select2 branch'),
				'constraints' => array(new NotBlank(array('message' => 'Please enter a branch.'))),
                            'query_builder' => function($repository) use ($userId) {
                            return $repository->findAllQb($userId);}
//				'query_builder' => function(EntityRepository $er) use ($regionId, $chainId, $userId) {
//					if (!empty($regionId) && !empty($chainId)) {
//						$qb = $er->createQueryBuilder('s')
//						->join('s.region', 'r')
//						->join('s.chain', 'c')
//						->leftJoin('c.parent', 'pc')
//						->where('r.id = :regionId')
//						->andWhere('c.id = :chainId OR pc.id = :chainId')
//						->setParameter('regionId', $regionId)
//						->setParameter('chainId', $chainId);
//						
//						if (!empty($userId)) {
//							$qb->andWhere('s.rsm = :userId or s.cdm = :userId or s.tl = :userId')
//								->setParameter('userId', $userId);
//						}
//					} else {
//						$qb = $er->createQueryBuilder('s')
//							->where('s.id = 0')
//						;
//					}
//					
//					return $qb;
//				}
			));
             };
		
		$renderProduct = function (Form $form, $storeId = null) {
			$form->add('product', 'entity', array(
				'class' => 'CIInventoryBundle:Product',
				'property' => 'name',
				'required' => false,
				'attr' => array('class' => 'select2 product'),
				'empty_value' => 'All products',
				'query_builder' => function(EntityRepository $er) use ($storeId) {
					if (!empty($storeId)) {
						return $er->createQueryBuilder('p')
						->join('p.stores', 's')
						->where('s.id = :storeId')
						->setParameter('storeId', $storeId)
						;
					} else {
						return $er->createQueryBuilder('p')->where('p.id = 0');
					}
				}
			));
		};
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($renderStore, $renderProduct) {
			$form = $event->getForm();
			$data = $event->getData();
				
			$regionId = $chainId = $storeId = null;
			
			if ($data) {
				if (array_key_exists('region', $data)) {
					$regionId = $data['region']->getId();
				}
					
				if (array_key_exists('chain', $data)) {
					$chainId = $data['chain']->getId();
				}
				
				if (array_key_exists('store', $data)) {
					$storeId = $data['store']->getId();
				}
			}
			
			$renderStore($form, $regionId, $chainId);
			
			$renderProduct($form, $storeId);
		});
		
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($renderStore, $renderProduct) {
			$form = $event->getForm();
			$data = $event->getData();
		
			$regionId = $chainId = $storeId = null;
				
			if (array_key_exists('region', $data)) {
				$regionId = $data['region'];
			}
				
			if (array_key_exists('chain', $data)) {
				$chainId = $data['chain'];
			}
	
			if (array_key_exists('store', $data)) {
				$storeId = $data['store'];
			}
				
			$renderStore($form, $regionId, $chainId);
				
			$renderProduct($form, $storeId);
		});
	}
	
	public function getName()
	{
		return 'ci_inventorybundle_branchosareportfilter';
	}
}