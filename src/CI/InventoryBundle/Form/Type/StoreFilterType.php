<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CI\CoreBundle\Entity\User;

use CI\InventoryBundle\Entity\Chain;
use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Store;

class StoreFilterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'required' => false,
			'attr' => array('widget_col' => 6)
		))
		->add('region', 'entity', array(
			'class' => 'CIInventoryBundle:Region',
			'empty_value' => 'All regions',
			'required' => false,
			'property' => 'name',
			'attr' => array('class' => 'select2', 'widget_col' => 6),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('chain', 'entity', array(
			'class' => 'CIInventoryBundle:Chain',
			'empty_value' => 'All chains',
			'required' => false,
			'property' => 'name',
			'attr' => array('class' => 'select2', 'widget_col' => 6),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('rsm', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'RSM',
			'empty_value' => 'Choose a RSM',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_RSM);
			}
		))
		->add('cdm', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'CDM',
			'empty_value' => 'Choose a CDM',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_CDM);
			}
		))
		->add('tl', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'TL',
			'empty_value' => 'Choose a TL',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_TL);
			}
		))
		->add('diser', 'entity', array(
			'class' => 'CICoreBundle:User',
			'label' => 'Diser',
			'empty_value' => 'Choose a diser',
			'property' => 'name',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
				return $er->findAllQb(User::ROLE_DISER);
			}
		))
                        
                           #jerome
                ->add('storeClassification', 'choice', array(
			'required' => false,
			'label' => 'Store Classification',
			'multiple' => true,
			'expanded' => true,
			'attr' => array('inline' => true),
			'choices' => array(
//				Store::KEY_ACCOUNT => 'Key Account',
				Store::OSA_TRACKED => 'Top Account'
			)
		))
                        
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_storefilter';
	}
}