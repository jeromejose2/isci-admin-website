<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints\NotBlank;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

use CI\CoreBundle\Entity\User;
#add jfj
use CI\InventoryBundle\Entity\Chain;
use CI\InventoryBundle\Entity\Product;
use CI\InventoryBundle\Entity\Store;

class DownloadOsaFilterType extends AbstractType
{
	protected $sc;
	
	public function __construct(SecurityContext $sc)
	{	
		$this->sc = $sc;
	}
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$sc = $this->sc;
    	$user = $sc->getToken()->getUser();
    	$userId = $user->getId();
    	$formOptions = array(
    		'class' => 'CICoreBundle:User',
    		'property' => 'name',
    		'required' => false,
    		'attr' => array('class' => 'select2')
    	);
    	 
    	if ($sc->isGranted(User::ROLE_ADMIN)) {
    		$userId = null;
    	
    		//rsm
    		$formOptions['label'] = "RSM";
    		$formOptions['empty_value'] = "All RSM";
    		$formOptions['query_builder'] = function($repository) {
    			return $repository->findAllQb(User::ROLE_RSM);
    		};
    		$builder->add('rsm', 'entity', $formOptions);
    	
    		//cdm
    		$formOptions['label'] = "CDM";
    		$formOptions['empty_value'] = "All CDM";
    		$formOptions['query_builder'] = function($repository) {
    			return $repository->findAllQb(User::ROLE_CDM);
    		};
    		$builder->add('cdm', 'entity', $formOptions);
    	
    		//tl
    		$formOptions['label'] = "TL";
    		$formOptions['empty_value'] = "All TL";
    		$formOptions['query_builder'] = function($repository) {
    			return $repository->findAllQb(User::ROLE_TL);
    		};
    		$builder->add('tl', 'entity', $formOptions);
    	
    		//diser
    		$formOptions['label'] = "Diser";
    		$formOptions['empty_value'] = "All Diser";
    		$formOptions['query_builder'] = function($repository) {
    			return $repository->findAllQb(User::ROLE_DISER);
    		};
    		$builder->add('diser', 'entity', $formOptions);
    	} else {
    		if ($sc->isGranted(User::ROLE_RSM)) {
    			$formOptions['label'] = "CDM";
    			$formOptions['empty_value'] = "All CDM";
    			$formOptions['query_builder'] = function($repository) use ($user) {
    				return $repository->getSubordinatesQb($user, User::ROLE_CDM);
    			};
    			$builder->add('cdm', 'entity', $formOptions);
    		}
    	
    		if ($sc->isGranted(array(new Expression('hasAnyRole("' . User::ROLE_RSM . '", "' . User::ROLE_CDM . '")')))) {
    			$formOptions['label'] = "TL";
    			$formOptions['empty_value'] = "All TL";
    			$formOptions['query_builder'] = function($repository) use ($user) {
    				return $repository->getSubordinatesQb($user, User::ROLE_TL);
    			};
    			$builder->add('tl', 'entity', $formOptions);
    		}
    	
    		if ($sc->isGranted(array(new Expression('hasAnyRole("' . User::ROLE_RSM . '", "' . User::ROLE_CDM . '", "' . User::ROLE_TL . '")')))) {
    			$formOptions['label'] = "Diser";
    			$formOptions['empty_value'] = "All Diser";
    			$formOptions['query_builder'] = function($repository) use ($user) {
    				return $repository->getSubordinatesQb($user, User::ROLE_DISER);
    			};
    			$builder->add('diser', 'entity', $formOptions);
    		}
    	}
    	
        $builder
        	->setMethod('GET')
        	->add('region', 'entity', array(
        		'class' => 'CIInventoryBundle:Region',
        		'property' => 'name',
        		'required' => false,
        		'empty_value' => 'All regions',
        		'attr' => array('class' => 'select2'),
        		'query_builder' => function($repository) use ($userId) {
        			return $repository->findAllQb($userId);
        		}
        	))
	        ->add('chain', 'entity', array(
        		'class' => 'CIInventoryBundle:Chain',
        		'property' => 'name',
				'required' => false,
	        	'empty_value' => 'All chains',
        		'attr' => array('class' => 'select2'),
        		'query_builder' => function($repository) use ($userId) {
        			return $repository->findAllQb(false, $userId);
        		}
        	))
			->add('dateFrom', 'date', array(
        		'label'    => 'Date From',
        		'required' => true,
				'constraints' => array(new NotBlank(array('message' => 'Please enter a date.'))),
        		'widget'   => 'single_text',
        		'format'   => 'MM/dd/y',
        		'attr'	   => array(
        			'widget_col' => 5,
        			'datepicker' => true,
        			'input_group' => array('append' => 'calendar'),
        		)
			))
        	->add('dateTo', 'date', array(
        		'label'    => 'Date To',
        		'required' => true,
        		'constraints' => array(new NotBlank(array('message' => 'Please enter a date.'))),
        		'widget'   => 'single_text',
        		'format'   => 'MM/dd/y',
        		'attr'	   => array(
        			'widget_col' => 5,
        			'datepicker' => true,
        			'input_group' => array('append' => 'calendar'),
        		)
        	))
                       #jfj add
//            ->add('chainClassification', 'choice', array(
//			'required' => false,
//			'label' => 'Chain Classification',
//			'multiple' => true,
//			'expanded' => true,
//			'attr' => array('inline' => true),
//			'choices' => array(
////				Chain::KEY_ACCOUNT => 'Key Account',
//				Chain::OSA_TRACKED => 'OSA Tracked'
//			)
//		))
            ->add('storeClassification', 'choice', array(
			'required' => false,
			'label' => 'Store Classification',
			'multiple' => true,
			'expanded' => true,
			'attr' => array('inline' => true),
			'choices' => array(
//				Store::KEY_ACCOUNT => 'Key Account',
				Store::OSA_TRACKED => 'Top Account'
			)
		))
            ->add('productClassification', 'choice', array(
        		'required' => false,
        		'label' => 'Product Classification',
        		'multiple' => true,
        		'expanded' => true,
        		'attr' => array('inline' => true),
        		'choices' => array(
        			Product::CORE => 'Core',
        			Product::NPD => 'NPD',
        			Product::SEASONAL => 'Seasonal',
        			Product::OSA_TRACKED => 'Promo Packs'//'OSA Tracked'
        		)
        	))
//                ->add('store', 'entity', array(
//        		'class' => 'CIInventoryBundle:Store',
//        		'property' => 'name',
//        		'label' => 'Branch',
//				'required' => false,
//	        	'empty_value' => 'All branches',
//        		'attr' => array('class' => 'select2'),
//        		'query_builder' => function($repository) use ($userId) {
//        			return $repository->findAllQb($userId);
//        		}
//        	))
                          #jfj add
                        
			->add('search', 'submit', array(
				'attr' => array(
					'class' => 'btn btn-outline submit-button',
					'data-loading-text' => "Searching..."
				)
			))
        ;
    }

    public function getName()
    {
        return 'ci_inventorybundle_downloadosafilter';
    }
}