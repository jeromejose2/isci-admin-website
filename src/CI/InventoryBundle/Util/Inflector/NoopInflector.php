<?php
namespace CI\InventoryBundle\Util\Inflector;

use FOS\RestBundle\Util\Inflector\InflectorInterface;

/**
 * Inflector class
 *
 */
class NoopInflector implements InflectorInterface
{
    public function pluralize($word)
    {
        return $word;
    }
}