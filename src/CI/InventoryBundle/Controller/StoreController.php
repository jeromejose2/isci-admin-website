<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * Store controller.
 *
 * @Route("/store")
 */
class StoreController extends BaseController
{
	const LINK_SHOW = 'store_show';
	
	public function getModel()
	{
		return $this->get('ci.store.model');
	}
	
	/**
	 * Shows the Store Listing.
	 *
	 * @Route("/", name="store")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
	
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Store entity.
	 *
	 * @Route("/", name="store_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Store:new.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('create'));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Displays a form to create a new Store entity.
	 *
	 * @Route("/new", name="store_new")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Finds and displays a Store entity.
	 *
	 * @Route("/{id}/show", name="store_show")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return array(
			'entity' => $entity
		);
	}
	
	/**
	 * Displays a form to edit an existing Store entity.
	 *
	 * @Route("/{id}/edit", name="store_edit")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}
	
	/**
	 * Edits an existing Store entity.
	 *
	 * @Route("/{id}", name="store_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Store:edit.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('update'));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}
	
	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="store_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * Deletes a Store entity.
	 *
	 * @Route("/{id}", name="store_delete")
	 * @Method("DELETE")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('delete'));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
		
		return $this->redirect($this->generateUrl('store'));
	}
	
	/**
	 * Confirm status change of a Store entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="store_confirm_change_status")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$name = '[Store] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
	
		return $this->confirmChangeStatus($id, $status, $name, 'store_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Store entity.
	 *
	 * @Route("/{id}/status-change", name="store_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function changeStatusAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $this->changeStatus($entity, 'store', self::LINK_SHOW);
	}
	
	/**
	 * @Route("/revision/{id}", name="store_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="store_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
	
	/**
	 * @Route("/{regionId}/{chainId}/get", requirements={"regionId"="\d+", "chainId"="\d+"}, name="store_branch_report_filter_get")
	 * @Method("GET")
	 * @PreAuthorize("hasAnyRole('ROLE_TL', 'ROLE_RSM', 'ROLE_CDM')")
	 */
	public function branchReportFilterAction($regionId, $chainId)
	{
		$model = $this->getModel();
		return new JsonResponse($model->getBranchReportFilter($regionId, $chainId), 200);
	}
}