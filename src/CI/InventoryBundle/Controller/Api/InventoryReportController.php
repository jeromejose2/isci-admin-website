<?php

namespace CI\InventoryBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CI\InventoryBundle\Helper\ApiResponse;
use CI\InventoryBundle\Helper\ApiUploadedFile;
use CI\InventoryBundle\Entity\UmReport;
use CI\InventoryBundle\Entity\InventoryReport;
use CI\InventoryBundle\Entity\UmReportItem;
use CI\InventoryBundle\Entity\InventoryReportItem;

/**
 * @NamePrefix("api_")
 */
class InventoryReportController extends FOSRestController
{

    public function getSaveapilog($method = '', $name = '', $desc = '', $status = '')
    {
  date_default_timezone_set('Asia/Manila');

        // start setlog
        $sql_log = "INSERT INTO api_log (name,method,description,status,date_created) VALUES (:name,:method,:description,:status,:date_created)";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql_log);
        $log_arr['name'] = $name;
        $log_arr['method'] = $method;
        $log_arr['description'] = $desc;
        $log_arr['status'] = $status;
        $log_arr['date_created'] = date('Y-m-d H:i:s');
        $stmt->execute($log_arr);
        // end setlog

        return true;
    }

    /**
     * @ApiDoc(
     * 		resource=true,
     * 		description="allows the request to POST a new resource (inventory report) on the server. (Does not validate if storeID is assigned to deviceID.)",
     *  	statusCodes={
     *         201="Returned when successful.",
     *         400="Returned if there are missing required values, validation errors or duplicate report.",
     *         403="Returned when the user is not authorized."
     *     }
     * )
     */
    public function postSavereportAction()
    {
        date_default_timezone_set('Asia/Manila');
        //validate appID, appSecret, deviceID
        $aas = $this->get('ci_inventory.api.authentication.service');
        $device = $aas->isAuthenticated($this->getRequest());

        $apiResponse = $this->get('ci.api.response');

        if (empty($device))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Access not granted.', array()
            );
            $view = $this->view($apiResponse, 403);
            return $this->handleView($view);
        }

        //decode json content
        $content = $this->getRequest()->getContent();
        $params = json_decode($content, true);


        $this->getSaveapilog('Savereport-start', $params['deviceID'], $content, 1);


        // validate request content
        if (empty($params['storeID']) or empty($params['reportDate']) or empty($params['createdDate']) or empty($params['userName']))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Some fields are missing or invalid. Please try again.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }

        /* start Created BY Jerome Jose
         * November 11 2015
         */

        //check role
        $checkRole = $this->getDoctrine()->getRepository('CIInventoryBundle:Device')->findRole($params['deviceID']);

        if (empty($checkRole))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'User not found.', array()
            );
            $view = $this->view($apiResponse, 403);
            return $this->handleView($view);
        }

        $device_role = $checkRole->getPersonInCharge()->getRole();







        if ('ROLE_ADMIN' == $device_role || 'ROLE_RSM' == $device_role || 'ROLE_CDM' == $device_role)
        {


            //check for duplicate report
            $invReport = $this->getDoctrine()->getRepository('CIInventoryBundle:UmReport')->getStoreReport_Um($params['storeID'], $params['reportDate']);

//            if ($invReport )
//            {
//                $apiResponse->setResponse(
//                        ApiResponse::ERROR, 'Upper Management Report for ' . $invReport[0]['s_name'] . ' for report date ' . date('M d, Y', strtotime(date_format($invReport[0]['um_reportDate'], 'Y-m-d H:i:s'))) . ' already exists.', array()
//                );
//                $view = $this->view($apiResponse, 400);
//                return $this->handleView($view);
//            }






            try
            {
                $this->getSaveapilog('Savereport-savingreport', $params['deviceID'], json_encode($params), 1);

                //save inventory report
                $photo = $this->saveInvReport_Um($device, $params);

                //send email comment in staging only to
//                $send = $this->sendEmail($invReport, $params, $device, $device_role, $checkRole,$photo);
//
//                if (!$send)
//                {
//                    throw new \Exception('Email not sent. Please try again.');
//                }

                $apiResponse->setResponse(
                        ApiResponse::SUCCESS, '', array()
                );
                $this->getSaveapilog('Savereport-end', $params['deviceID'], json_encode($params), 1);

                $view = $this->view($apiResponse, 200);
                return $this->handleView($view);
            } catch (\Exception $e)
            {
                $error_messge = $e->getMessage();
                $apiResponse->setResponse(
                        ApiResponse::ERROR, $e->getMessage(), array()
                );

                $this->getSaveapilog('Savereport-error', $params['deviceID'], $error_messge, 2);

                $view = $this->view($apiResponse, 400);
                return $this->handleView($view);
            }
        } elseif ('ROLE_TL' == $device_role || 'ROLE_DISER' == $device_role)
        {

            //check for duplicate report
            $invReport = $this->getDoctrine()->getRepository('CIInventoryBundle:InventoryReport')->getStoreReport($params['storeID'], $params['reportDate']);

            if ($invReport)
            {
                $apiResponse->setResponse(
                        ApiResponse::ERROR, 'Report for ' . $invReport[0]['storeName'] . ' for report date ' . date('M d, Y', strtotime($invReport[0]['reportDate'])) . ' already exists.', array()
                );

                $view = $this->view($apiResponse, 400);
                return $this->handleView($view);
            }

            try
            {
                //save inventory report
                $this->getSaveapilog('Savereport-savingreport', $params['deviceID'], json_encode($params), 1);

                $this->saveInvReport($device, $params);

                $apiResponse->setResponse(
                        ApiResponse::SUCCESS, '', array()
                );
                $this->getSaveapilog('Savereport-end', $params['deviceID'], json_encode($params), 1);

                $view = $this->view($apiResponse, 200);
                return $this->handleView($view);
            } catch (\Exception $e)
            {
                $error_messge = $e->getMessage();
                $apiResponse->setResponse(
                        ApiResponse::ERROR, $error_messge, array()
                );
                $this->getSaveapilog('Savereport-error', $params['deviceID'], $error_messge, 2);

                $view = $this->view($apiResponse, 400);
                return $this->handleView($view);
            }
        } else
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Invalid User Role', array()
            );
            $view = $this->view($apiResponse, 403);
            return $this->handleView($view);
        }

        /* end Created BY Jerome Jose
         * November 11 2015
         */
    }

    private function processImage_UM($obj, $data)
    {
        if (isset($data['photo']) && !empty($data['photo']))
        {
            $file = new ApiUploadedFile($data['photo']);

            $obj->setFile($file);
        }
    }

    /**
     * Creates new InventoryReport entity.
     * @param array $data
     */
    private function saveInvReport_Um($device, $data)
    {
        $invReport = new UmReport(); // for um report
        $invReport->setDevice($device);

        $storeModel = $this->get('ci.store.model');
        $store = $storeModel->findExistingEntity($data['storeID']);
        $invReport->setStore($store);

        if ($data['reportDate'])
        {
            $invReport->setReportDate(new \DateTime($data['reportDate']));
        }
        if ($data['createdDate'])
        {
            $invReport->setReportCreationDate(new \DateTime($data['createdDate']));
        }
        if ($data['lastModifiedDate'])
        {
            $invReport->setReportLastUpdatedDate(new \DateTime($data['lastModifiedDate']));
        }

        $invReport->setCreatedBy($data['userName']);
        $invReport->setNote($data['note']);

        //process invReport photo
        $this->processImage_UM($invReport, $data);

        foreach ($data['products'] as $product)
        {
            $hasError = false;

            $invReportItem = new UmReportItem();

            $productModel = $this->get('ci.product.model');
            try
            {
                $productObj = $productModel->findExistingEntity($product['id']);
            } catch (\Exception $e)
            {
                // product not found
                $hasError = true;
            }

            $invReportItem->setProduct($productObj);
            $invReportItem->setAvailable($product['available']);
            $invReportItem->setCount($product['count']);
            if (isset($product['note']))
            {
                $invReportItem->setNote($product['note']);
            }

            //process the image
            $this->processImage($invReportItem, $product);
            $invReport->addItem($invReportItem);
        }

        //validate InventoryReport entity
        $validator = $this->get('validator');
        $errors = $validator->validate($invReport);

        //format error messages as string, if any
        if (count($errors) > 0 or $hasError)
        {
// 			$messages = array();
// 			foreach ($errors as $error) {
// 				$message = $error->getMessage();
// 				if (!in_array($message, $messages)) {
// 					$messages[] = $message;
// 				}
// 			}
            throw new \Exception('Some fields are missing or invalid. Please try again.');
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($invReport);
        $em->flush();
        return $invReport->getFile();
    }

    private function saveInvReport($device, $data)
    {
        $invReport = new InventoryReport();
//		$invReport = new UmReport(); // for um report
        $invReport->setDevice($device);

        $storeModel = $this->get('ci.store.model');
        $store = $storeModel->findExistingEntity($data['storeID']);
        $invReport->setStore($store);

        if ($data['reportDate'])
        {
            $invReport->setReportDate(new \DateTime($data['reportDate']));
        }
        if ($data['createdDate'])
        {
            $invReport->setReportCreationDate(new \DateTime($data['createdDate']));
        }
        if ($data['lastModifiedDate'])
        {
            $invReport->setReportLastUpdatedDate(new \DateTime($data['lastModifiedDate']));
        }

        $invReport->setCreatedBy($data['userName']);
        $invReport->setNote($data['note']);

        //process invReport photo
        $this->processImage($invReport, $data);

        foreach ($data['products'] as $product)
        {
            $hasError = false;

            $invReportItem = new InventoryReportItem();

            $productModel = $this->get('ci.product.model');
            try
            {
                $productObj = $productModel->findExistingEntity($product['id']);
            } catch (\Exception $e)
            {
                // product not found
                $hasError = true;
            }

            $invReportItem->setProduct($productObj);
            $invReportItem->setAvailable($product['available']);
            $invReportItem->setCount($product['count']);
            if (isset($product['note']))
            {
                $invReportItem->setNote($product['note']);
            }

            //process the image
            $this->processImage($invReportItem, $product);

            $invReport->addItem($invReportItem);
        }

        //validate InventoryReport entity
        $validator = $this->get('validator');
        $errors = $validator->validate($invReport);

        //format error messages as string, if any
        if (count($errors) > 0 or $hasError)
        {
// 			$messages = array();
// 			foreach ($errors as $error) {
// 				$message = $error->getMessage();
// 				if (!in_array($message, $messages)) {
// 					$messages[] = $message;
// 				}
// 			}
            throw new \Exception('Some fields are missing or invalid. Please try again.');
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($invReport);
        $em->flush();
    }

    /**
     * Save image.
     * @param InventoryReportItem/InventoryReport $obj
     * @param array $data
     */
    private function processImage($obj, $data)
    {
        if (isset($data['photo']) && !empty($data['photo']))
        {
            $file = new ApiUploadedFile($data['photo']);

            $obj->setFile($file);
        }
    }

    private function sendEmail($invReport, $data, $device, $device_role, $checkRole, $photo)
    {

        function clean_string($string)
        {
            $bad = array("content-type", "bcc:", "to:", "cc:", "href");
            return str_replace($bad, "", $string);
        }

        $storeModel = $this->get('ci.store.model');
        $store = $storeModel->findExistingEntity($data['storeID']);

        //test
        // the message
        $email_cc_from = "";
        if ('ROLE_ADMIN' == $device_role)
        {
            $email_cc_from = "";
        }

        if ('ROLE_RSM' == $device_role)
        {
            $email_cc_from = "" . $store->getAdm()->getName() . "<" . $store->getAdm()->getEmail() . ">";
        }

        if ('ROLE_CDM' == $device_role)
        {
            $email_cc_from = "" . $store->getAdm()->getName() . "<" . $store->getAdm()->getEmail() . ">";
            $email_cc_from .= "," . $store->getRsm()->getName() . "<" . $store->getRsm()->getEmail() . ">";
        }


        $sender_name = $data['userName'];
        $device_sender_name = $device->getName();
        $storename = $store->getName();
        $reportDate = date('M d, Y', strtotime($data['reportDate']));
        $datenow = date('M d, Y');
        $notes = $data['note'];
        $reportsender_date = date('M d, Y', strtotime($data['createdDate']));

//        $msg = <<<EOT
//
//
//        Store $storename
//        has been visited by $sender_name  last $datenow.
//        Report was sent $reportsender_date.
//        See below:
//        Product Name   |  Availability
//EOT;
//
//
//
//
//
//        $msg .= <<<EOT
//
//        Notes: $notes
//
//EOT;
//
//// use wordwrap() if lines are longer than 70 characters
//        $msg = wordwrap($msg, 70);
// send email
        $email_from = "ISCI OSA System Admin <no-reply@isci.com>";
        $email_to = '' . $device->getPersonInCharge()->getName() . ' <' . $device->getPersonInCharge()->getEmail() . '>';
        $headers = 'From: ' . clean_string($email_to) . "\r\n" .
                'Reply-To: ' . clean_string($email_from) . "\r\n" .
                'Cc:' . clean_string($email_cc_from) . "\r\n" .
                'X-Mailer: PHP/' . phpversion() . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";







        $email_subject = "Stock Availability Report:" . $storename . " " . $reportDate;

        $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>A Simple Responsive HTML Email</title>
        <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}
        </style>
    </head>
    <body yahoo bgcolor="#f6f8f1">
        <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
        Store ' . $storename . '
        has been visited by ' . $device->getPersonInCharge()->getName() . '  last ' . $reportDate . '.
                            </td>
                        </tr>

                        <tr>
                            <td>
        Report was sent ' . $datenow . '.
        See below:
                            </td>
                        </tr>
                        <tr><td><strong>Product</strong> </td><td><strong>Availability</strong></td></tr>';

        foreach ($data['products'] as $product)
        {
            $productModel = $this->get('ci.product.model');
            $productObj = $productModel->findExistingEntity($product['id']);
            $product_name = $productObj->getName();
            $prod_available = ($product['available'] == 1) ? 'Yes' : 'No';
            $msg .='<tr><td>' . $product_name . ' </td><td>' . $prod_available . '</td></tr>';
        }

        $msg .= <<<EOT

        Notes: $notes

EOT;
        $msg .='</table>
                </td>
            </tr>
        </table>';

//          $msg .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
//			$msg .= "<tr style='background: #eee;'><td>Store ".$storename." has been visited by ".$sender_name."  last ".$datenow."</td></tr>";
//			$msg .= "<tr><td> Report was sent $reportsender_date. </td></tr>";
//			$msg .= "<tr><td><strong>Type of Change:</strong> </td><td>" . strip_tags($email_from) . "</td></tr>";
//			$msg .= "<tr><td><strong>Urgency:</strong> </td><td>" . strip_tags($email_from) . "</td></tr>";
//			$msg .= "<tr><td><strong>URL To Change (main):</strong> </td><td>" . $email_from. "</td></tr>";
//
//
//			$msg .= "<tr><td><strong>NEW Content:</strong> </td><td>" . htmlentities($email_from) . "</td></tr>";
//			$msg .= "</table>";


        $msg .= '  </body>
</html>';


//     $data['photo'] = 'C:\xampp\htdocs\isci\web\uploads\inventory-report\1\axdas.jpeg';
//     $data['photo'] = '/home/storyteching/public_html/dev.isci/web/uploads/inventory-report/13/ea3448e44df8cb36a3fc86c6b2446c7f7143b3d8.jpeg';
//     $data['photo'] = '/var/www/isci/isci/web/uploads/inventory-report/10636/e148cc3b964e5e66aef52bec0f1d929a66f44264.jpeg';


        /*  start
         * using MAIL PHP
         */
        if (isset($photo) && !empty($photo))
        {


            $file = $photo;

            $file_size = filesize($file);

            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $name = basename($file);



            // send email
            $email_from = "ISCI OSA System Admin <no-reply@isci.com>";
            $email_to = '' . $device->getPersonInCharge()->getName() . ' <' . $device->getPersonInCharge()->getEmail() . '>';
            $headers = 'From: ' . clean_string($email_to) . "\r\n" .
                    'Reply-To: ' . clean_string($email_from) . "\r\n" .
                    'Cc:' . clean_string($email_cc_from) . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n";
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            //        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";



            // message & attachment
            $nmessage = "--" . $uid . "\r\n";
            $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
            $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $nmessage .= $msg . "\r\n\r\n";
            $nmessage .= "--" . $uid . "\r\n";
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $file . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"photo-" . $uid . ".jpg\"\r\n\r\n";
            $nmessage .= $content . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";

            mail($email_to, $email_subject, $nmessage, $headers);
        } else
        {
            mail($email_to, $email_subject, $msg, $headers);
        }

        /*
         * using SWIFT MAILER
         */
        $mailer = $this->container->get('mailer');
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername('osa.isci@gmail.com')
                ->setPassword('NWP@ssw0rd');

        $mailer = \Swift_Mailer::newInstance($transport);

        $from = array('jerome.jose@nuworks.ph' => 'ISCI OSA System Admin');

        $message = \Swift_Message::newInstance('Test')
                ->setSubject($email_subject)
                ->setFrom($from)
                ->setTo($email_to)
                ->setCc(clean_string($email_cc_from))
                ->setBody($msg);
        $message->setContentType("text/html");
        if (isset($photo) && !empty($photo))
        {
            $message->attach(\Swift_Attachment::fromPath($photo));
        }

        $test = $this->get('mailer')->send($message);

        return true;


        /*  end
         * using MAIL PHP
         */
    }

}
