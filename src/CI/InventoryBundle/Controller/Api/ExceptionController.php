<?php

namespace CI\InventoryBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use CI\InventoryBundle\Helper\ApiResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

class ExceptionController extends FOSRestController
{	
    public function errorAction()
    {
    	$apiResponse = $this->get('ci.api.response');
    	
    	$apiResponse->setResponse(
    		ApiResponse::ERROR, 
    		'Request body format is invalid or is not supported.',
    		array()
    	);
    	
    	$view = $this->view($apiResponse, 400);
    	return $this->handleView($view);

    }
}