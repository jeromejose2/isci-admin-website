<?php

namespace CI\InventoryBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use CI\InventoryBundle\Entity\Device;
use CI\InventoryBundle\Helper\ApiResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use CI\CoreBundle\Model\UserModel;

/**
 * @NamePrefix("api_")
 */
class AppUpdateController extends FOSRestController
{

    /**
     * 
     * @ApiDoc(
     * 		description="Checks if the device has the latest app and latest data version",
     *  	statusCodes={
     *         200="Returned when successful.",
     *         400="Returned when required values are not present.",
     *         403="Returned when the device is not authorized."
     * 		},
     *  	parameters={
     *      	{"name"="appVersion", "dataType"="string", "required"=true, "description"="app version"},
     *      	{"name"="dataVersion", "dataType"="string", "required"=true, "description"="data version"},
     *  	}
     * )
     */
    public function getAppupdateAction()
    {

     

        //validate appID, appSecret, deviceID
        $aas = $this->get('ci_inventory.api.authentication.service');
        $device = $aas->isAuthenticated($this->getRequest());

        $apiResponse = $this->get('ci.api.response');

        if (empty($device))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Access not granted.', array()
            );
            $view = $this->view($apiResponse, 403);
            return $this->handleView($view);
        }

   $this->apilog('appupdate-start',  $device->getDeviceID(),'', 1);
       
        $model = $this->get('ci.mobileapp.model');
        $mobile = $model->findExistingEntity();

        $clientAppVersion = $this->getRequest()->get('appVersion');
        $clientDataVersion = $this->getRequest()->get('dataVersion');

        // return a 400 if app version or data version were not passed
        if (empty($clientAppVersion) or empty($clientDataVersion))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Missing App Version and Data Version.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }

        // check if client's app version or data version matches with the current app version and data version
        $data = array();
        if ($clientAppVersion !== $mobile->getAppVersion())
        {
            $data['app'] = array(
                'latestAppVersion' => $mobile->getAppVersion(),
                'downloadLink' => $mobile->getWebPath(),
                'pin' => $mobile->getPin()
            );
        }

        if ($clientDataVersion !== $mobile->getDataVersion())
        {
            $data['data'] = array(
                'latestDataVersion' => $mobile->getDataVersion()
            );
        }
 
      
  $this->apilog('appupdate-end', $device->getDeviceID(), json_encode( $data), 1);


        $apiResponse->setResponse(
                ApiResponse::SUCCESS, empty($data) ? 'No updates.' : 'An update is available.', $data
        );

        $view = $this->view($apiResponse, 200);

        return $this->handleView($view);
    }

    public function getUserlistAction()
    {
        //$content = $this->getRequest()->getContent();

$device_id = $this->getRequest()->headers->get('device_id');
       // return a 400 if app version or data version were not passed
        if (empty($device_id))
        {
         
          $device_id = $this->getRequest()->headers->get('deviceID');
                if (empty($device_id))
                {
                     $device_id = '';
                }
        }

        $apiResponse = $this->get('ci.api.response');

      $this->apilog('userlist-start', $device_id, '', 1); 
   

        $sql = " 
        SELECT device.name,device_id 
          FROM user inner join device on user.id = device.personInCharge_id;
    ";

        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $user_all = $stmt->fetchAll();

        $apiResponse->setResponse(
                ApiResponse::SUCCESS, '', $user_all
        );
   $this->apilog('userlist-end',$device_id, json_encode( $user_all), 1);

        $view = $this->view($apiResponse, 200);
        return $this->handleView($view);
    }

    /*
     * Status = 1 - success on api 2 -erro on api 3 - success on mobile 4- error on mobile
     */

    public function postSaveapilogAction()
    {



        date_default_timezone_set('Asia/Manila');

        $apiResponse = $this->get('ci.api.response');
        $content = $this->getRequest()->getContent();
        $params = json_decode($content, true);

        if (empty($params['name']) or empty($params['method']) or empty($params['description']) or empty($params['date_created']) or empty($params['status']))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Some fields are missing or invalid. Please try again.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }

        try
        {
            // start setlog
            $sql_log = "INSERT INTO api_log (name,method,description,status,date_created) VALUES (:name,:method,:description,:status,:date_created); ";
            $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql_log);
            $log_arr['name'] = $params['name'];
            $log_arr['method'] = $params['method'];
            $log_arr['description'] = $params['description'];
            $log_arr['status'] = $params['status'];
            $log_arr['date_created'] = date('Y-m-d H:i:s', strtotime($params['date_created']));
            $stmt->execute($log_arr);
            // end setlog

            $apiResponse->setResponse(
                    ApiResponse::SUCCESS, '', array()
            );

            $view = $this->view($apiResponse, 200);
            return $this->handleView($view);
        } catch (Exception $e)
        {
            $error_messge = $e->getMessage();
            $apiResponse->setResponse(
                    ApiResponse::ERROR, $e->getMessage(), array()
            );

            $this->apilog('savelog-error', $params['deviceID'], $error_messge, 2);

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }
    }

    public function apilog($method = '', $name = '', $desc = '', $status = '')
    {
  date_default_timezone_set('Asia/Manila');

        // start setlog
        $sql_log = "INSERT INTO api_log (name,method,description,status,date_created) VALUES (:name,:method,:description,:status,:date_created); ";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql_log);
        $log_arr['name'] = $name;
        $log_arr['method'] = $method;
        $log_arr['description'] = $desc;
        $log_arr['status'] = $status;
        $log_arr['date_created'] = date('Y-m-d H:i:s');
        $stmt->execute($log_arr);
        // end setlog

        return true;
    }

    public function postSavedeviceidAction()
    {
        date_default_timezone_set('Asia/Manila');

        $apiResponse = $this->get('ci.api.response');
        $content = $this->getRequest()->getContent();
        $params = json_decode($content, true);

        $this->apilog('savedeviceId-start', $params['device_id'], $content, 1);
        
//        $this->apilog('savedeviceId-start', $params['device_id'], $content, 1);
        // validate request content
        if (empty($params['new_device_id']) or empty($params['device_id']) or empty($params['name']))
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Some fields are missing or invalid. Please try again.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }

        $sql = " 
        SELECT device.name,device_id ,user.id,device.old_device_id
          FROM user inner join device on user.id = device.personInCharge_id where device.device_id = :deviceid and name = :name ;
    ";
        $where = array();
        $where['deviceid'] = $params['device_id'];
        $where['name'] = $params['name'];

        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute($where);

        $get_old_device = $stmt->fetchAll();
        if (!$get_old_device)
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Old Device ID not found. Please try again.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }
        $old_device_id_array_1 = '';
        if ($get_old_device[0]['old_device_id'] == null)
        {
            $old_device_id_array_1 = array($get_old_device[0]['old_device_id']);
            $old_device_id_array = array($get_old_device[0]['device_id'] => date('Y-m-d H:i:s'));
            array_push($old_device_id_array_1, $old_device_id_array);
            $old_device_id_array_1 = json_encode($old_device_id_array_1);
        } else
        {
            $old_device_id_array_1 = json_decode($get_old_device[0]['old_device_id']);
            $old_device_id_array = array($get_old_device[0]['device_id'] => date('Y-m-d H:i:s'));
            array_push($old_device_id_array_1, $old_device_id_array);
            $old_device_id_array_1 = json_encode($old_device_id_array_1);
        }



        $where['newdeviceid'] = $params['new_device_id'];
        $where['olddeviceidarray'] = $old_device_id_array_1;

        $sql = "UPDATE device SET old_device_id=:olddeviceidarray,device_id=:newdeviceid WHERE device_id=:deviceid  and name = :name ";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $query1 = $stmt->execute($where);

        if (!$query1)
        {
            $apiResponse->setResponse(
                    ApiResponse::ERROR, 'Error. Please try again.', array()
            );

            $view = $this->view($apiResponse, 400);
            return $this->handleView($view);
        }

        $apiResponse->setResponse(
                ApiResponse::SUCCESS, '', array()
        );

        $this->apilog('savedeviceId-end', $params['device_id'], json_encode($where, true), 1);

        $view = $this->view($apiResponse, 200);
        return $this->handleView($view);
    }

    public function getAllapilogAction()
    {
        date_default_timezone_set('Asia/Manila');

        $apiResponse = $this->get('ci.api.response');
//        $content = $this->getRequest()->getContent();
        $where = '';
        if (isset($_GET['deviceid']) && $_GET['deviceid'] != '' || isset($_GET['method']) && $_GET['method'] != '' || isset($_GET['date']) && $_GET['date'] != '')
        {
            $where .= ' WHERE ';
        }
        if (isset($_GET['deviceid']) && $_GET['deviceid'] != '')
        {
            $name = $_GET['deviceid'];
            if (preg_match('/' . preg_quote('^\'£$%^&*()}{@#~?><,@|-=-_+-¬', '/') . '/', $name))
            {
                $apiResponse->setResponse(
                        ApiResponse::ERROR, 'Error. Please try again.', array()
                );

                $view = $this->view($apiResponse, 400);
                return $this->handleView($view);
            }
            $where .= ' name like "%' . $name . '%" ';
        }

        if (isset($_GET['method']) && $_GET['method'] != '')
        {
            $method = $_GET['method'];
            if (preg_match('/' . preg_quote('^\'£$%^&*()}{@#~?><,@|-=-_+-¬', '/') . '/', $method))
            {
                $apiResponse->setResponse(
                        ApiResponse::ERROR, 'Error. Please try again.', array()
                );

                $view = $this->view($apiResponse, 400);
                return $this->handleView($view);
            }

            if (isset($_GET['deviceid']) && $_GET['deviceid'] != '')
            {
                $where .= ' AND method like "%' . $method . '%" ';
            } else
            {
                $where .= ' method like "%' . $method . '%" ';
            }
        }

        if (isset($_GET['date']) && $_GET['date'] != '')
        {
            if (isset($_GET['deviceid']) && $_GET['deviceid'] != '')
            {
                $where .= ' AND DATE(date_created) = DATE("' . $_GET['date'] . '") ';
            } else
            {
                $where .= ' DATE(date_created) = DATE("' . $_GET['date'] . '") ';
            }
        }



        $sql = " 
        SELECT * 
          FROM api_log " . $where;



        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();

        if ($records)
        {
            if (isset($_GET['download']) && $_GET['download'] != 0)
            {
                $myFile = 'logs/'.date('Y-M-d-His-')."api_log.txt";
                $fh = fopen($myFile, 'w') or die("can't open file");
//            foreach ($records as $key => $value)
//            {

                fputcsv($fh, array('id,DeviceId/Name,Description,DateCreated'));
                $data = array();
                foreach ($records as $line)
                {
//                $data[] = $line;
                    fputcsv($fh, $line, "\t");
                }
                fclose($fh);

                $csvData = file_get_contents($myFile);

                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename=' . $myFile);
                header('Content-Length: ' . strlen($csvData));
                echo $csvData;
            } else
            {
//                print_r('<pre>');
//                print_r($records);
                
                
		$apiResponse->setResponse(
			ApiResponse::SUCCESS,
			'',
			$records
		);
		
		$view = $this->view($apiResponse, 200);
		
		return $this->handleView($view);
                
            }
            exit;
//            }
        } else
            exit;
    }

}
