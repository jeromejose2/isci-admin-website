<?php
namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * EntityAudit controller
 *
 * @Route("/activity-log")
 * @PreAuthorize("hasRole('ROLE_ADMIN')")
 */
class ActivityLogController extends Controller
{
	/**
	 * Activity Log
	 *
	 * @Route("/", name="activitylog")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction()
	{
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$this->getDoctrine()->getManager()->getRepository('CIInventoryBundle:ActivityLog')->findAll(),
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page')
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null 
		);
	}
}