<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * ProductReport controller.
 *
 * @Route("/report/product")
 */
class ProductReportController extends BaseController
{
	public function getModel()
	{
		return $this->get('ci.product.report.model');
	}
	
    /**
     * @Route("/core-average", name="get_core_average_report")
     * @Method("GET")
     * @Template()
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function getCoreAverageReportAction(Request $request)
    {
        $model = $this->getModel();
        $form = $model->getCoreAverageReport('filter');
        
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
                              
				$results = $model->getCoreAverageReport('index', $params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}

        return array(
        	'params' => isset($params) ? $params : null,
			'results' => isset($results) ? $results : null,
			'search_form' => $form->createView(),
        );
    }
    
    /**
     * @Route("/core-average/export/xls", name="get_core_average_report_export_xls")
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function getCoreAverageReportExportXlsAction(Request $request)
    {
    	$model = $this->getModel();
        $form = $model->getCoreAverageReport('filter');
    	$form->handleRequest($request);
    	$params = $form->getData();
    	$data = $model->getCoreAverageReport('xls', $params);
    	
    	$response = new Response();
    	$response->headers->set('Content-Type', 'application/vnd.ms-excel');
    	$response->headers->set('Content-Disposition', 'attachment;filename='. $data['filename'] . '.xls');
    	$response->headers->set('Cache-Control', 'ax-age=0');
    	$response->sendHeaders();
    
    	$data['objWriter']->save('php://output');
    	exit();
    }
    
    /**
     * @Route("/frequency-vs-ok", name="get_frequency_vs_ok_report")
     * @Method("GET")
     * @Template()
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function getFrequencyVsOkReportAction(Request $request)
    {
    	$model = $this->getModel();
    	$form = $model->getFrequencyVsOkReport('filter');
    
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			$params = $form->getData();
    			$results = $model->getFrequencyVsOkReport('index', $params);
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
    		}
    	}
    
    	return array(
    			'params' => isset($params) ? $params : null,
    			'results' => isset($results) ? $results : null,
    			'search_form' => $form->createView(),
    	);
    }
    
    /**
     * @Route("/frequency-vs-ok/export/xls", name="get_frequency_vs_ok_report_export_xls")
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function getFrequencyVsOkReportExportXlsAction(Request $request)
    {
    	$model = $this->getModel();
    	$form = $model->getFrequencyVsOkReport('filter');
    	$form->handleRequest($request);
    	$params = $form->getData();
    	$data = $model->getFrequencyVsOkReport('xls', $params);
    	 
    	$response = new Response();
    	$response->headers->set('Content-Type', 'application/vnd.ms-excel');
    	$response->headers->set('Content-Disposition', 'attachment;filename='. $data['filename'] . '.xls');
    	$response->headers->set('Cache-Control', 'ax-age=0');
    	$response->sendHeaders();
    
    	$data['objWriter']->save('php://output');
    	exit();
    }
}