<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Model\ProductModel as Model;
use CI\InventoryBundle\Entity\Product;

/**
 * Product controller.
 *
 * @Route("/product")
 * @PreAuthorize("hasRole('ROLE_ADMIN')")
 */
class ProductController extends BaseController
{
	const LINK_SHOW = 'product_show';
	
	public function getModel()
	{
		return $this->get('ci.product.model');
	}
	
    /**
     * Lists all Product.
     *
     * @Route("/", name="product")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request) 
    {
    	$model = $this->getModel();
    	$form = $model->getFilterFormType();
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			$params = $form->getData();
    			$qb = $model->getIndex($params);
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
    		}
    	}
    	
    	if (!isset($qb)) {
    		$qb = $model->getIndex();
    	}
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$qb,
    		$this->get('request')->query->get('page', 1),
    		$this->container->getParameter('pagination_limit_per_page'),
    		array('distinct' => true)
    	);
    	
    	return array(
    		'pagination' => isset($pagination) ? $pagination : null,
    		'search_form' => $form->createView()
    	);
    }

    /**
     * Creates a new Product entity.
     *
     * @Route("/", name="product_create")
     * @Method("POST")
     * @Template("CIInventoryBundle:Product:new.html.twig")
     */
    public function createAction(Request $request) 
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			try {
    				$model->saveEntity($form, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_CREATE));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    	);
    }

    /**
     * Displays a form to create a new Product entity.
     *
     * @Route("/new", name="product_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    	
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView()
    	);
    }

    /**
     * Finds and displays a Product entity.
     *
     * @Route("/{id}/show", requirements={"id"="\d+"}, name="product_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    		
    	return array(
    		'entity' => $entity
    	);
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     * @Route("/{id}/edit", requirements={"id"="\d+"}, name="product_edit")
     * @Method("GET")
     */
    public function editAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isEditable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$editForm = $model->getFormType($entity);
    	
    	return $this->render('CIInventoryBundle:Product:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }

    /**
     * Edits an existing Product entity.
     *
     * @Route("/{id}", requirements={"id"="\d+"}, name="product_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isEditable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$editForm = $model->getFormType($entity);
    	
    	if ($editForm->handleRequest($request)->isSubmitted()) {
    		if ($editForm->isValid()) {
    			try {
    				$model->saveEntity($editForm, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return $this->render('CIInventoryBundle:Product:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }

    /**
     * Deletes a Product entity.
     *
     * @Route("/{id}/delete", requirements={"id"="\d+"}, name="product_confirm_delete")
     * @Template("CIInventoryBundle:Misc:delete.html.twig")
     */
    public function confirmDeleteAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isDeletable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$deleteForm = $this->createDeleteForm($id);    		
    	return array(
    		'entity' => $entity,
    		'delete_form' => $deleteForm->createView(),
    		'params' => $model->getDeleteParams($entity)
    	);
    }
    
    /**
     * @Route("/{id}", requirements={"id"="\d+"}, name="product_delete")
     * @Method("DELETE")
     * @Template("CIInventoryBundle:Misc:delete.html.twig")
     */
    public function deleteAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isDeletable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$form = $this->createDeleteForm($id);
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			try {
    				$model->deleteEntity($id);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
    		}
    	}
    	
    	return $this->redirect($this->generateUrl('product'));
    }
    
    /**
     * Confirm status change of a Product entity.
     *
     * @Route("/{id}/confirm-change-status/{status}", name="product_confirm_change_status")
     * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
     */
    public function confirmChangeStatusAction($id, $status)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$name = '[Product] '. $entity->getName() . ' (ID# ' . $entity->getId() . ')';
    
    	return $this->confirmChangeStatus($id, $status, $name, 'product_status_change', self::LINK_SHOW);
    }
    
    /**
     * Change status of a Product entity.
     *
     * @Route("/{id}/status-change", name="product_status_change")
     * @Method("PUT")
     * @Template("CIInventoryBundle:Misc:changeStatus.html.twig")
     */
    public function changeStatusAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	return $this->changeStatus($entity, 'product', self::LINK_SHOW);
    }
    
    /**
     * @Route("/revision/{id}", requirements={"id"="\d+"}, name="product_revision")
     * @Template("CIInventoryBundle:Log:index.html.twig")
     */
    public function revisionAction($id)
    {
    	$model = $this->getModel();
    	$revision = $model->getRevision($id);
    	return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
    /**
     * @Route("/revision/{parentId}/{id}/log", requirements={"id"="\d+", "parentId"="\d+"}, name="product_log")
     * @Template("CIInventoryBundle:Log:show.html.twig")
     */
    public function logAction($parentId, $id)
    {
    	$model = $this->getModel();
    	$log = $model->getLog();
    	return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
    }
    
    /**
     * @Route("/{branchId}/get", requirements={"branchId"="\d+"}, name="product_branch_report_filter_get")
     * @Method("GET")
     */
    public function branchReportFilterAction($branchId)
    {
    	$model = $this->getModel();
    	return new JsonResponse($model->getBranchReportFilter($branchId), 200);
    }
}