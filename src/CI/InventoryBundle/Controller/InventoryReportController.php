<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use PHPExcel;
use PHPExcel_IOFactory;
use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Form\Type\InventoryReportFilterType;

/**
 * InventoryReport controller
 *
 * @Route("/inventory-report")
 */
class InventoryReportController extends Controller
{

    /**
     * InventoryReport
     *
     * @Route("/", name="inventoryreport")
     * @Method("GET")
     * @Template()
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new InventoryReportFilterType($this->get('security.context')));

        if ($form->handleRequest($request)->isSubmitted())
        {
            if ($form->isValid())
            {
                $params = $form->getData();
                $params = $this->prepareParams($params);

                $qb = $em->getRepository('CIInventoryBundle:InventoryReport')->findAll($params);
            } else
            {
                $this->get('session')->getFlashBag()->add('danger', 'Please try again.');
            }
        }

        if (isset($qb))
        {
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                    $qb, $this->get('request')->query->get('page', 1), $this->container->getParameter('pagination_limit_per_page'), array('distinct' => true)
            );
        }

        return array(
            'params' => isset($params) ? $params : null,
            'pagination' => isset($pagination) ? $pagination : null,
            'search_form' => $form->createView(),
        );
    }

    private function prepareParams(array $params)
    {
        $sc = $this->get('security.context');

        if (!$sc->isGranted(User::ROLE_ADMIN))
        {
            if ($sc->isGranted(User::ROLE_RSM))
            {
                $params['rsm'] = $this->getUser();
            } else if ($sc->isGranted(User::ROLE_CDM))
            {
                $params['cdm'] = $this->getUser();
            } else if ($sc->isGranted(User::ROLE_TL))
            {
                $params['tl'] = $this->getUser();
            } else if ($sc->isGranted(User::ROLE_DISER))
            {
                $params['diser'] = $this->getUser();
            }
        }

        return $params;
    }

    /**
     * @Route("/export/xls", name="inventoryreport_export_xls")
     * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
     */
    public function exportXLSAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new InventoryReportFilterType($this->get('security.context')));
        $form->handleRequest($request);
        $params = $form->getData();
        $params = $this->prepareParams($params);

        $objPHPExcel = new PHPExcel();

        $fontStyle = array('bold' => true);

        $styleArray = array(
            'font' => $fontStyle,
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
        );

        $objPHPExcel->getProperties()->setCreator("ISCI")->setTitle("Inventory Report");

        $xls = $objPHPExcel->setActiveSheetIndex(0);

        $xls->getColumnDimension("A")->setAutoSize(true);
        $xls->getColumnDimension("B")->setAutoSize(true);
        $xls->getColumnDimension("C")->setAutoSize(true);
        $xls->getColumnDimension("D")->setAutoSize(true);
//		$xls->getColumnDimension("E")->setAutoSize(true);
        $reports = $em->getRepository("CIInventoryBundle:InventoryReport")->findAll($params)->getResult();

        $counter = 1;


        $count = 1;
        $count_prod = 1;
        foreach ($reports as $report)
        {
            if ($count == 1)
            {
                $xls->setCellValue('A' . $counter, ' ');


                foreach ($report->getItems() as $item)
                {
                    if ($count_prod == 1)
                    {
                        $xls->setCellValue('B' . $counter, $item->getProduct()->getName());
                    } else
                    {
                        break;
                    }
                    $count_prod++;
                }

                $xls->setCellValue('C' . $counter, '');
                $xls->setCellValue('D' . $counter, '');
            } else
            {
                break;
            }
            $count ++;
        }
 $xls->getStyle("A" . $counter . ":E" . $counter)->applyFromArray($styleArray + array('borders' => array('top' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
            $counter++;
                $xls->setCellValue('A' . $counter, '');
                $xls->setCellValue('B' . $counter, '');
                $xls->setCellValue('C' . $counter, '');
                $xls->setCellValue('D' . $counter, '');
                
                
        $counter++;

        $xls->setCellValue('A' . $counter, 'Branch');
        $xls->setCellValue('B' . $counter, 'Date of Input');
        $xls->setCellValue('C' . $counter, 'Available');
        $xls->setCellValue('D' . $counter, 'Quantity');
//			$xls->setCellValue('E' . $counter, 'Username');

 $xls->getStyle("A" . $counter . ":E" . $counter)->applyFromArray($styleArray + array('borders' => array('top' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
           
        foreach ($reports as $report)
        {

            $counter++;

            $date = "Report Date: " . $report->getReportDate()->format('M. d, Y') . "\n" .
                    "Creation Date: " . $report->getReportCreationDate()->format('M. d, Y') . "\n" .
                    "Upload Date: " . $report->getCreatedAt()->format('M. d, Y');

            if ($report->getReportLastUpdatedDate())
            {
                $date .= "\nLast Updated Date: " . $report->getReportLastUpdatedDate()->format('M. d, Y');
            }

            $xls->setCellValue('A' . $counter, $report->getStore()->getName());
            $xls->getStyle('A' . $counter)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true);
            $xls->setCellValue('B' . $counter, $report->getReportDate()->format('M. d, Y'));


            //C AVAILABLE
            foreach ($report->getItems() as $item)
            {
                $xls->setCellValue('C' . $counter, $item->getAvailable() ? 'Yes' : 'No');
//                              $counter++;
            }


//			$xls->setCellValue('D' . $counter, $date);
            //D COUNT
            foreach ($report->getItems() as $item)
            {
                $count = $item->getCount() ? number_format($item->getCount(), 2) : 'No Input';
                $xls->setCellValue('D' . $counter, $count);
//				$counter++;
            }

            $xls->getStyle('D' . $counter)->getAlignment()->setWrapText(true);
//			$xls->setCellValue('E' . $counter, $report->getCreatedBy());
//			$counter++;
//			$xls->setCellValue('A' . $counter, 'Products');
//			$counter++;
//			$xls->setCellValue('A' . $counter, 'Name');
//			$xls->setCellValue('B' . $counter, 'Available');
//			$xls->setCellValue('C' . $counter, 'Count');
//			$xls->setCellValue('D' . $counter, 'Note');
//			$xls->getStyle("A" . ($counter - 1) . ":D" . $counter)->applyFromArray($styleArray);
//			$counter++;
//			
//			foreach($report->getItems() as $item) {
//				$xls->setCellValue('A' . $counter, $item->getProduct()->getName());
//				$xls->setCellValue('B' . $counter, $item->getAvailable() ? 'Yes' : 'No');
//				$count = $item->getCount() ? number_format($item->getCount(), 2) : 'No Input';
//				$xls->setCellValue('C' . $counter, $count);
//				$xls->getStyle('C' . $counter)->getNumberFormat()->setFormatCode('#,##0.00;[Red](#,##0.00)');
//				$xls->setCellValue('D' . $counter, $item->getNote());
//				$counter++;
//			}
            $counter++;
        }

        $xls->setSelectedCell();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $response = new Response();
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');

        $filename = 'Inventory_Report_' . $params['dateFrom']->format('Y-m-d') .'_'.$params['dateTo']->format('Y-m-d');

        $response->headers->set('Content-Disposition', 'attachment;filename=' . $filename . '.xls');
        $response->headers->set('Cache-Control', 'ax-age=0');
        $response->sendHeaders();

        $objWriter->save('php://output');
        exit();
    }

    public function exportXLSAction_BAKUP(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new InventoryReportFilterType($this->get('security.context')));
        $form->handleRequest($request);
        $params = $form->getData();
        $params = $this->prepareParams($params);

        $objPHPExcel = new PHPExcel();

        $fontStyle = array('bold' => true);

        $styleArray = array(
            'font' => $fontStyle,
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
        );

        $objPHPExcel->getProperties()->setCreator("ISCI")->setTitle("Inventory Report");

        $xls = $objPHPExcel->setActiveSheetIndex(0);

        $xls->getColumnDimension("A")->setAutoSize(true);
        $xls->getColumnDimension("B")->setAutoSize(true);
        $xls->getColumnDimension("C")->setAutoSize(true);
        $xls->getColumnDimension("D")->setAutoSize(true);
        $xls->getColumnDimension("E")->setAutoSize(true);

        $reports = $em->getRepository("CIInventoryBundle:InventoryReport")->findAll($params)->getResult();

        $counter = 1;
        foreach ($reports as $report)
        {
            $xls->setCellValue('A' . $counter, 'Report ID123');
            $xls->setCellValue('B' . $counter, 'Device ID123');
            $xls->setCellValue('C' . $counter, 'Branch');
            $xls->setCellValue('D' . $counter, 'Dates');
            $xls->setCellValue('E' . $counter, 'Username');
            $xls->getStyle("A" . $counter . ":E" . $counter)->applyFromArray($styleArray + array('borders' => array('top' => array('style' => \PHPExcel_Style_Border::BORDER_THIN))));
            $counter++;

            $date = "Report Date: " . $report->getReportDate()->format('M. d, Y') . "\n" .
                    "Creation Date: " . $report->getReportCreationDate()->format('M. d, Y') . "\n" .
                    "Upload Date: " . $report->getCreatedAt()->format('M. d, Y');

            if ($report->getReportLastUpdatedDate())
            {
                $date .= "\nLast Updated Date: " . $report->getReportLastUpdatedDate()->format('M. d, Y');
            }

            $xls->setCellValue('A' . $counter, $report->getId() . "\n" . $report->getNote());
            $xls->getStyle('A' . $counter)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true);
            $xls->setCellValue('B' . $counter, $report->getDevice()->getDeviceId());
            $xls->setCellValue('C' . $counter, $report->getStore()->getName());
            $xls->setCellValue('D' . $counter, $date);
            $xls->getStyle('D' . $counter)->getAlignment()->setWrapText(true);
            $xls->setCellValue('E' . $counter, $report->getCreatedBy());
            $counter++;

            $xls->setCellValue('A' . $counter, 'Products');
            $counter++;
            $xls->setCellValue('A' . $counter, 'Name');
            $xls->setCellValue('B' . $counter, 'Available');
            $xls->setCellValue('C' . $counter, 'Count');
            $xls->setCellValue('D' . $counter, 'Note');
            $xls->getStyle("A" . ($counter - 1) . ":D" . $counter)->applyFromArray($styleArray);
            $counter++;

            foreach ($report->getItems() as $item)
            {
                $xls->setCellValue('A' . $counter, $item->getProduct()->getName());
                $xls->setCellValue('B' . $counter, $item->getAvailable() ? 'Yes' : 'No');
                $count = $item->getCount() ? number_format($item->getCount(), 2) : 'No Input';
                $xls->setCellValue('C' . $counter, $count);
                $xls->getStyle('C' . $counter)->getNumberFormat()->setFormatCode('#,##0.00;[Red](#,##0.00)');
                $xls->setCellValue('D' . $counter, $item->getNote());
                $counter++;
            }
            $counter++;
        }

        $xls->setSelectedCell();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $response = new Response();
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');

        $filename = 'Inventory_Report_' . date('M-d-Y');

        $response->headers->set('Content-Disposition', 'attachment;filename=' . $filename . '.xls');
        $response->headers->set('Cache-Control', 'ax-age=0');
        $response->sendHeaders();

        $objWriter->save('php://output');
        exit();
    }

}
