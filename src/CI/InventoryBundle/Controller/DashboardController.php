<?php
namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\InventoryBundle\Model\DashboardModel as Model;

/**
 * Dashboard controller
 *
 */
class DashboardController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.dashboard.model');
	}
	
    /**
     * Dashboard
     *
     * @Route("/", name="dashboard")
     * @Template()
     */
    public function indexAction()
    {
    	$model = $this->getModel();
    	return $model->getIndex();
    }
}