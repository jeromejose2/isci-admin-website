<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CI\InventoryBundle\Entity\BaseEntity;

/**
 * Base controller.
 *
 */
class BaseController extends Controller
{
	const PREFIX = 'CI\InventoryBundle\Entity\\';
	
	/**
	 * Get doctrine entity manager
	 * 
	 */
	protected function getEm()
	{
		return $this->getDoctrine()->getManager();
	}
	
    /**
     * Confirm status change of an entity.
     *
     */
    protected function confirmChangeStatus($id, $status, $name, $target_route, $return_route, $alternateReturnRoute = null, $alternateReturnId = null)
    {
        return array(
        	'alternate_return_id' => $alternateReturnId,
        	'status' => $status,
        	'name' => $name,
        	'target_route' => $target_route,
        	'return_route' => is_null($return_route) ? $alternateReturnRoute : $return_route,
            'change_status_form' => $this->createChangeStatusForm(array(
            	'id' => $id, 
            	'status' => $status
            ))->createView(),
        );
    }
    
    public function changeStatus($entity, $entityName, $targetRoute, $alternateTargetRoute = null, $alternateTargetId = null)
    {
    	$changeStatusForm = $this->createChangeStatusForm();
    	$changeStatusForm->bind($this->getRequest());
    	    	
    	if ($changeStatusForm->isValid()) {
    		$newStatus = $changeStatusForm->get('status')->getData();
    		
    		if ($newStatus === BaseEntity::STATUS_INACTIVE) {
    			$setActive = false;
    		} else { 
    			$setActive = true;
    		}
    		
    		if ($setActive !== $entity->isActive()) {
    			$entity->setActive($setActive);
    			$em = $this->getEm();
    			$em->persist($entity);
    			$em->flush();

    			$this->get('session')->getFlashBag()->add('success', sprintf('This %s has been set to %s.', $entityName, $newStatus));
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', sprintf('This %s is already %s.', $entityName, $newStatus));
    		}
    		
    	} else {
    		$this->get('session')->getFlashBag()->add('danger', 'Sorry, unable to change the status. Please try again.');
    	}
    	
    	return $this->redirect($this->generateUrl(is_null($targetRoute) ? $alternateTargetRoute : $targetRoute, 
    			array('id' => !is_null($alternateTargetId) ? $alternateTargetId : $entity->getId())));
    }
    
    protected function createChangeStatusForm($params = array())
    {
    	return $this->createFormBuilder($params)
    		->add('status', 'hidden')
	    	->add('id', 'hidden')
	    	->getForm()
	    	;
    }
    
    public function createDeleteForm($id)
    {
    	return $this->createFormBuilder(array('id' => $id))
	    	->add('id', 'hidden')
	    	->setMethod('DELETE')
	    	->getForm()
    	;
    }
    
    /**
     * Revisions listing page.
     * @param string $routePrefix
     * @param string $name
     * @param array $classes
     */
    public function entityRevision($routePrefix, $name, $class, $viewId = null, $viewRoute = null)
    {
    	$auditReader = $this->container->get("ci_core.entity_audit.reader");
    	
    	//parent revisions
    	$parentRevisions = $auditReader->findRevisions(self::PREFIX . $class['class'], $class['id']);
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    			$parentRevisions,
    			$this->get('request')->query->get('page', 1),
    			$this->container->getParameter('pagination_limit_per_page')
    	);
    
    	return array(
    		'pagination' => $pagination,
    		'return_id' => $class['id'],
    		'view_id' => $viewId,
    		'view_route' => $viewRoute,
    		'route_prefix' => $routePrefix,
    		'name' => $name
    	);
    }
    
    /**
     * Show the changed entities in a specific revision.
     * @param int $returnId
     * @param string $routePrefix
     * @param int $id
     * @param string $name
     * @param array $classes
     */
    public function entityLog($returnId, $routePrefix, $id, $name, $classes, $viewId = null, $viewRoute = null)
    {
    	$auditReader = $this->container->get("ci_core.entity_audit.reader");
    	$oldRev = $this->get('request')->query->get('old');
    	
    	// get revision information
    	$rev = $auditReader->findRevision($id);
    	
    	// get entity revision values
    	$changedEntities = $auditReader->findEntitiesChangedAtRevision($id, $classes);
    	
    	$logs = array();
    	foreach ($changedEntities as $entity) {
    		$diff = $auditReader->diff($entity->getClassName(), $entity->getId(), $oldRev, $entity->getEntity());
    		
    		$logs[] = array(
    			'entity_changes' => $diff,
    			'action' => $entity->getRevisionType());
    	}
    	
    	return array(
    		'logs'=> $logs,
    		'route_prefix' => $routePrefix,
    		'return_id' => $returnId,
    		'view_id' => $viewId,
    		'view_route' => $viewRoute,
    		'username' => $rev->getUsername(),
    		'timestamp' => $rev->getTimestamp(),
    		'name' => $name
    	);
    }
}
