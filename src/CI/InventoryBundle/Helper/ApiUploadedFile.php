<?php

namespace CI\InventoryBundle\Helper;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Used to decode base64 images. 
 */
class ApiUploadedFile extends File
{
    public function __construct($base64)
    {
    	//create temp file
        $filePath = tempnam(sys_get_temp_dir(), 'UploadedFile');
        //open file
        $file = fopen($filePath, 'w');
        //convert base64 image
        stream_filter_append($file, 'convert.base64-decode');
        fwrite($file, $base64);
        //get image uri
        $meta_data = stream_get_meta_data($file);
        $path = $meta_data['uri'];
        //close file
        fclose($file);

        parent::__construct($path, true);
    }

}