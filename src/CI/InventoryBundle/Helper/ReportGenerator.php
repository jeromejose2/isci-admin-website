<?php

namespace CI\InventoryBundle\Helper;

use CI\InventoryBundle\Helper\CustomTCPDF;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ReportGenerator
{  
	private $container;
	
	public function __construct(Container $container) 
	{
		$this->container = $container;
	}	
	public function exportReport($name, $params, $rows, $template, $filename = null, $type = CustomTCPDF::REPORT)
	{
		$pdf = $this->container->get("white_october.tcpdf")->create();
		$pdf->setFormName($name);
		$pdf->setType($type);
		$pdf->AddPage();
		
		$html = $this->container->get('templating')->render($template, array(
			'rows' => $rows,
			'params' => $params
		));
		
		$pdf->writeHTML($html);
		$pdf->lastPage();

		if (empty($filename)) {
			$filename = str_ireplace(' ', '_', $name) . '_' . date('M-d-Y');
		}
		
		$response = new Response($pdf->Output($filename, 'I'));
		$response->headers->set('Content-Type', 'application/pdf');		
	}
	
	public function exportCellReport($name, $preparedParams, $rows, $tableFormat, $filename = null, $type = null, $misc = array())
	{
    	$pdf = $this->container->get("white_october.tcpdf")->create();
    	$pdf->setFormName($name);
    	
    	$pdf->setType($type);
    	$pdf->SetFont('helvetica', 'B', 8);
    	$type === CustomTCPDF::LANDSCAPE ? $pdf->AddPage('L') : $pdf->AddPage();
    	
    	$pageMargins = $pdf->getMargins();
    	$pageWidth = $pdf->getPageWidth() - $pageMargins['left'] - $pageMargins['right'];
    	
    	foreach ($preparedParams as $param => $value) {
    		if (!is_null($value)) {
    			    $pdf->write(5, $param . ': ' . $value, '', false, 'L', true);
    		}
    	}
    	$pdf->Ln();
    	
    	foreach ($tableFormat as $format) {
    		$pdf->Cell($format["width"] * $pageWidth, 5, $format["header"], 1, 0, 'C', false, '', 0, false, 'T', 'C');
    	}
    	$pdf->Ln();
    	
    	if (count($rows) > 0) {
    	    $counter = 0;
        	foreach ($rows as $row) {
        		foreach ($row as $key => $col) {
        			if (isset($misc['excludeColumns']) && in_array($key, $misc['excludeColumns'])) {
        				continue;
        			}
        			
        			if (isset($tableFormat[$counter]["total"])) {
        				$tableFormat[$counter]["total"] += $col;
        			}
        			 
        					
        			$col = $tableFormat[$counter]["number"] === null ? $col : number_format((float)$col, $tableFormat[$counter]["number"]);
        			$pdf->Cell($tableFormat[$counter]["width"] * $pageWidth, 5, $col, 0, 0, $tableFormat[$counter]["align"], false, '', 0, false, 'T', 'C');
        			$counter++;
        		}
        		$pdf->Ln();
        		$counter = 0;
        	}
        	
        	$pdf->SetFont('helvetica', 'B', 10);
        	foreach ($tableFormat as $format) {
        		$value = isset($format['total']) ? number_format((float)$format['total'], $format["number"]) : '';
        		$pdf->Cell($format["width"] * $pageWidth, 7, $value, 0, 0, $format["align"], false, '', 0, false, 'T', 'C');
        	}
    	} else {
    	    $pdf->write(10, 'No results found.', '', false, 'C');
    	}
    	$pdf->lastPage();
    	
    	if (empty($filename)) {
    	    $filename = str_ireplace(' ', '_', $name) . '_' . date('M-d-Y');
    	}
    	
    	$response = new Response($pdf->Output($filename, 'I'));
    	$response->headers->set('Content-Type', 'application/pdf');
	}
}