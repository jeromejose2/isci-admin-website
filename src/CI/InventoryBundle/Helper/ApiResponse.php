<?php

namespace CI\InventoryBundle\Helper;

use Doctrine\ORM\EntityManager;

class ApiResponse
{
	const SUCCESS = 0;
	const ERROR = 1;
	
	private $error;
	
	private $message;
	
	private $data = array();
	
	public function setResponse($error, $message, $data)
	{
		$this->error = $error;
		$this->message = $message;
		$this->data = $data;
		
		return $this;
	}

	public function set($key, $value)
	{
		array_merge($this->data, array($key => $value));
		
		return $this;
	}

	public function hasError()
	{
		return $this->error == self::ERROR ? true : false;
	}
}