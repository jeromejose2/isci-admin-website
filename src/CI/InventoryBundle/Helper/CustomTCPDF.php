<?php
namespace CI\InventoryBundle\Helper;

use TCPDF;

class CustomTCPDF extends TCPDF 
{
	const FORM = 2;
	const REPORT = 1; 
	const LOGO = 3;
	const LANDSCAPE = 'L';
		
	protected $title;
	protected $formNo;
	protected $type;
	protected $options;
 
	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false)
	{
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
		$this->SetMargins(10, 50, 10, true);
		$this->SetHeaderMargin(10);
		$this->SetFooterMargin(10);
		$this->SetFont('helvetica', '', 10);
	}
	
	public function setType($type, $options = null)
	{
		switch($type) {
			case self::LOGO:
				$this->type = self::LOGO;
				$this->SetMargins(10, 30, 10, true);
				break;
			case CustomTCPDF::FORM: 
				$this->type = self::FORM;
				$this->SetMargins(10, 40, 10, true);
				break;
			case self::LANDSCAPE:
				$this->type = self::LANDSCAPE;
				$this->SetMargins(10, 30, 10, true);
				break;
			default:
				$this->type = CustomTCPDF::REPORT;
				$this->SetMargins(10, 35, 10, true);
		}
	}
	
	public function setFormName($formName)
	{
		$this->formName = $formName;
 	}
 	
 	public function setFormNo($formNo)
 	{
 		$this->formNo = $formNo;
 	}
 
	public function Header()
 	{
 		if ($this->type == self::LOGO) {
 			//display image here
 		} elseif ($this->type === self::FORM) {
 			$this->SetFont('helvetica', 'B', 20);
 			$this->Write(0, $this->formName, '', false, 'R', true);
 			$this->SetFont('helvetica', '', 10);
 			$this->Text(149, $this->getY(), 'Date: '.date('M d, Y g:i:s a'), false, false, true, 0, 1);
 			$this->Text(149, $this->getY(), 'Page: ' . $this->PageNo().' of '.$this->getAliasNbPages());
 		} elseif ($this->type === self::LANDSCAPE) {
 			$this->SetFont('helvetica', 'B', 17);
 			$this->Write(0, $this->formName, '', false, 'R', true);
 			$this->SetFont('helvetica', '', 10);
 			$this->Text(225, $this->getY(), 'Report Date: '.date('M d, Y g:i:s a'), false, false, true, 0, 1);
 			$this->Text(225, $this->getY(), 'Page: ' . $this->PageNo().' of '.$this->getAliasNbPages());
 		} else {
 			$this->SetFont('helvetica', 'B', 17);
 			$this->Write(0, $this->formName, '', false, 'R', true);
 			$this->SetFont('helvetica', '', 10);
 			$this->Text(138, $this->getY(), 'Report Date: '.date('M d, Y g:i:s a'), false, false, true, 0, 1);
 			$this->Text(138, $this->getY(), 'Page: ' . $this->PageNo().' of '.$this->getAliasNbPages());
 		}
 	}
 
	public function Footer()
	{		
		if ($this->getPage() > 1) {
	         $this->SetFont('helvetica', 'I', 8);
	         $this->Write(0, 'Page '.$this->PageNo().' of '.$this->getAliasNbPages(), '', false, 'C');
  		}
    }
}