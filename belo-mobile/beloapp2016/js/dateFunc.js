var dateFunc = {};

dateFunc.isLeapYear = function (year) { 
    return (year % 4 === 0);
};

dateFunc.getDaysInMonth = function (year, month) {
    month++; var day31 = [1,3,5,7,8,10,12]; var day30 = [4,6,9,11]; if (day31.inArray(month)){return 31} else if (day30.inArray(month)){return 30} else {if (dateFunc.isLeapYear(year)){return 29} else {return 28}};
};

dateFunc.getDayName = function(num){var ref = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']; return ref[num]};

dateFunc.getMonthName = function(num){var ref = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; return ref[num]};

Date.prototype.isLeapYear = function () { 
    return dateFunc.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return dateFunc.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (months) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(parseInt(this.getMonth()) + parseInt(months));
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};


Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
};